<?php
require_once('./app/web/config.php');

require_once('app/web/routes.php');
require_once('app/helpers/ViewHelper.php');
require_once('app/helpers/Lang.php');
require_once('app/helpers/UrlHelper.php');
require_once('app/helpers/VerifyCsrf.php');
require_once('app/helpers/Pagination.php');

spl_autoload_register(function ($className) {
$className=str_replace('\\','/',$className);
    require_once('./'.$className . ".php");
});

$path = $_SERVER['REQUEST_URI'];
$url = rtrim($path, '/');
$url = filter_var($url, FILTER_SANITIZE_URL);
$url = explode('/', $url);

$route_key = isset($url[3]) ? ($url[3] ? $url[3] : '/') : '/';
$route_key = explode('?', $route_key)[0];

// var_dump(encrypt("1"));
// die();
//var_dump(encrypt("kamil"));
if (!in_array($url[2], ['admin', 'api', 'app'])) {
    if (in_array($url[2], $allowed_langs)) {
        $_SESSION['lang'] = $url[2];
    } else {
        if (!isset($_SESSION['lang'])) {
            $_SESSION['lang'] = $default_lang;
            $url = 'Location: ' . $first_url . '/' . $default_lang;
            header($url);
        } elseif (!$_SESSION['lang']) {
            $_SESSION['lang'] = $default_lang;
            $url = 'Location: ' . $first_url . '/' . $default_lang;
            header($url);
        } else {
            $get_lang = $_SESSION['lang'];

            if (!in_array($get_lang, $allowed_langs)) {
                $_SESSION['lang'] = $default_lang;
                $url = 'Location: ' . $first_url . '/' . $default_lang;
                header($url);
            } else {
                $url = 'Location: ' . $first_url . '/' . $get_lang;
                header($url);
            }
        }
    }
}
//var_dump($route_key);
if (isset($url[2])) {
    if ($url[2] == 'admin') {
        if (isset($admin_routes[$route_key])) {
            //            var_dump("Kamil");
            $request_method = $_SERVER['REQUEST_METHOD'];
            $controllerName = $admin_routes[$route_key]['controller'];
            $controller = new $controllerName();
            $method = $admin_routes[$route_key][$request_method]['method_name'];

            //            var_dump($method);
            if (method_exists($controller, $method)) {
                // if ($request_method == "POST") {
                //     if (!isset($_POST['_token'])) {
                //         die("<h1>404 Not Found Token yoxdur</h1>");
                //     } elseif ($_POST['_token'] != $_SESSION['csrf_token']) {
                //         die("<h1>404 Not Found Token sehvdir</h1>");
                //     }
                // }
                if (isset($admin_routes[$route_key]['middleware']) && isset($admin_routes[$route_key]['middleware_method'])) {

                    $midd = $admin_routes[$route_key]['middleware'];
                    $mid = new $midd();
                    $m_method = $admin_routes[$route_key]['middleware_method'];

                    if (method_exists($mid, $m_method)) {
                        $mid->{$m_method}();
                    }
                }
                if (isset($url[4])) {
                    //                    var_dump($url[4]);
                    $controller->{$method}($url[4]);
                } else {
                    $controller->{$method}(false);
                }
            } else {
                echo "<h1>404 Not Found 1</h1>";
            }
        } else {
            echo "<h1>404 Not Found 2</h1>";
        }
    } else if ($url[1] == 'app') {
        //        var_dump($url);
        //        die();
    } else if ($url[2] == 'api') {
    } elseif (in_array($url[2], $allowed_langs)) {
        if (isset($routes[$route_key])) {
            $request_method = $_SERVER['REQUEST_METHOD'];
            $controllerName = $routes[$route_key]['controller'];
            $controller = new $controllerName();
            $method = $routes[$route_key][$request_method]['method_name'];

            //            var_dump($method);
            if (method_exists($controller, $method)) {
                if ($request_method == "POST") {
                    if (!isset($_POST['_token'])) {
                        die("<h1>404 Not Found Token yoxdur</h1>");
                    } elseif ($_POST['_token']) {
                        if (decrypt($_POST['_token']) != $csrf_key) {
                            die("<h1>404 Not Found Token sehvdir</h1>");
                        }
                    }
                }
                if (isset($routes[$route_key]['middleware']) && isset($routes[$route_key]['middleware_method'])) {

                    $midd = $routes[$route_key]['middleware'];
                    $mid = new $midd();
                    $m_method = $routes[$route_key]['middleware_method'];

                    if (method_exists($mid, $m_method)) {
                        $mid->{$m_method}();
                    }
                }
                if (isset($url[4])) {
                    //                    var_dump($url[3]);
                    $controller->{$method}($url[4]);
                } else {
                    $controller->{$method}(false);
                }
            } else {
                echo "<h1>404 Not Found 1</h1>";
            }
        } else {
            echo "<h1>404 Not Found 234</h1>";
        }
    } else {
        echo "<h1>Error 404 not found</h1>";
    }
} else {
    $_SESSION['lang'] = $default_lang;
    header("Location:$main_domain$main_directory/$default_lang");
}
