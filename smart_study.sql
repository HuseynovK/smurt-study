-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 09, 2021 at 11:42 AM
-- Server version: 8.0.17
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smart_study`
--

-- --------------------------------------------------------

--
-- Table structure for table `sm_blog`
--

CREATE TABLE `sm_blog` (
  `blog_id` int(11) NOT NULL,
  `blog_author` varchar(255) NOT NULL,
  `blog_image` varchar(255) NOT NULL,
  `blog_author_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `blog_title` varchar(255) NOT NULL,
  `blog_context` text NOT NULL,
  `blog_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sm_country`
--

CREATE TABLE `sm_country` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `country_context` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `country_photo_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `admin_id` int(11) NOT NULL,
  `country_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sm_course`
--

CREATE TABLE `sm_course` (
  `course_id` int(11) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `course_image` varchar(255) NOT NULL,
  `course_context` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sm_education`
--

CREATE TABLE `sm_education` (
  `education_id` int(11) NOT NULL,
  `education_name` varchar(255) NOT NULL,
  `education_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sm_message`
--

CREATE TABLE `sm_message` (
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `message_surname` varchar(255) NOT NULL,
  `message_phone` varchar(255) NOT NULL,
  `message_context` text NOT NULL,
  `message_type` varchar(255) NOT NULL,
  `message_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sm_service`
--

CREATE TABLE `sm_service` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `service_image` varchar(255) NOT NULL,
  `service_single_image` varchar(255) NOT NULL,
  `service_context` varchar(255) NOT NULL,
  `service_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sm_success`
--

CREATE TABLE `sm_success` (
  `success_id` int(11) NOT NULL,
  `success_fullname` varchar(255) NOT NULL,
  `success_image` varchar(255) NOT NULL,
  `success_uni` varchar(255) NOT NULL,
  `success_faculty` varchar(255) NOT NULL,
  `success_degree` varchar(255) NOT NULL,
  `success_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `email`, `password`, `token`) VALUES
(1, 'Ferman', 'fermanallahverdiyev31@gmail.com', 'NedUlko2fePCAlX+deNA0lB4F5dlsTLsAyzNJc8jVn7rszGVhwnm0OayWeOZ5IWPKeCtrp3vU/8pxSWTI2gOjA==', '123456');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sm_blog`
--
ALTER TABLE `sm_blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `sm_country`
--
ALTER TABLE `sm_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `sm_course`
--
ALTER TABLE `sm_course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `sm_education`
--
ALTER TABLE `sm_education`
  ADD PRIMARY KEY (`education_id`);

--
-- Indexes for table `sm_message`
--
ALTER TABLE `sm_message`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `sm_service`
--
ALTER TABLE `sm_service`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `sm_success`
--
ALTER TABLE `sm_success`
  ADD PRIMARY KEY (`success_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sm_blog`
--
ALTER TABLE `sm_blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sm_country`
--
ALTER TABLE `sm_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sm_course`
--
ALTER TABLE `sm_course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sm_education`
--
ALTER TABLE `sm_education`
  MODIFY `education_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sm_message`
--
ALTER TABLE `sm_message`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sm_service`
--
ALTER TABLE `sm_service`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sm_success`
--
ALTER TABLE `sm_success`
  MODIFY `success_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
