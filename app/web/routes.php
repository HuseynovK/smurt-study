<?php

$routes = [
    '/' => [
        'controller' => 'app\controller\MainController',
        'GET' => [
            'method_name' => 'index'
        ],
        'POST' => [
            'method_name' => 'message'
        ],
        'middleware' => 'app\middlewares\HomeMidlleware',
        'middleware_method' => 'index'
    ],
    'login' => [
        'controller' => 'app\controller\AuthController',
        'GET' => [
            'method_name' => 'login'
        ],
        'POST' => [
            'method_name' => 'signin'
        ],
        'middleware' => 'app\middlewares\HomeMidlleware',
        'middleware_method' => 'index'
    ],
    'blog_detail' => [
        'controller' => 'app\controller\MainController',
        'GET' => [
            'method_name' => 'blog_detail'
        ],
        'POST' => [
            'method_name' => ''
        ],
        'middleware' => 'app\middlewares\HomeMidlleware',
        'middleware_method' => 'index',
    ],
    'blog' => [
        'controller' => 'app\controller\MainController',
        'GET' => [
            'method_name' => 'blog'
        ],
        'POST' => [
            'method_name' => ''
        ],
        'middleware' => 'app\middlewares\HomeMidlleware',
        'middleware_method' => 'index',
    ],
    'country' => [
        'controller' => 'app\controller\MainController',
        'GET' => [
            'method_name' => 'country'
        ],
        'POST' => [
            'method_name' => ''
        ],
        'middleware' => 'app\middlewares\HomeMidlleware',
        'middleware_method' => 'index',
    ],
    'country_detail' => [
        'controller' => 'app\controller\MainController',
        'GET' => [
            'method_name' => 'country_detail'
        ],
        'POST' => [
            'method_name' => ''
        ],
        'middleware' => 'app\middlewares\HomeMidlleware',
        'middleware_method' => 'index',
    ],
    'service_detail' => [
        'controller' => 'app\controller\MainController',
        'GET' => [
            'method_name' => 'service_detail'
        ],
        'POST' => [
            'method_name' => ''
        ],
        'middleware' => 'app\middlewares\HomeMidlleware',
        'middleware_method' => 'index',
    ],
    'success' => [
        'controller' => 'app\controller\MainController',
        'GET' => [
            'method_name' => 'success'
        ],
        'POST' => [
            'method_name' => ''
        ],
        'middleware' => 'app\middlewares\HomeMidlleware',
        'middleware_method' => 'index',
    ],
    'course_detail' => [
        'controller' => 'app\controller\MainController',
        'GET' => [
            'method_name' => 'course_detail'
        ],
        'POST' => [
            'method_name' => ''
        ],
        'middleware' => 'app\middlewares\HomeMidlleware',
        'middleware_method' => 'index',
    ],
];

$admin_routes = [
    '/' => [
        'controller' => 'app\controller\AdminController',
        'GET' => [
            'method_name' => 'index'
        ],
        'POST' => [
            'method_name' => 'about'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],

    'country' => [
        'controller' => 'app\controller\ap\CountryController',
        'GET' => [
            'method_name' => 'list'
        ],
        'POST' => [
            'method_name' => ''
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'add_country' => [
        'controller' => 'app\controller\ap\CountryController',
        'GET' => [
            'method_name' => 'add'
        ],
        'POST' => [
            'method_name' => 'store'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'edit_country' => [
        'controller' => 'app\controller\ap\CountryController',
        'GET' => [
            'method_name' => 'edit'
        ],
        'POST' => [
            'method_name' => 'update'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'delete_country' => [
        'controller' => 'app\controller\ap\CountryController',
        'GET' => [
            'method_name' => ''
        ],
        'POST' => [
            'method_name' => 'delete'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],

    'service' => [
        'controller' => 'app\controller\ap\ServiceController',
        'GET' => [
            'method_name' => 'list'
        ],
        'POST' => [
            'method_name' => ''
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'add_service' => [
        'controller' => 'app\controller\ap\ServiceController',
        'GET' => [
            'method_name' => 'add'
        ],
        'POST' => [
            'method_name' => 'store'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'edit_service' => [
        'controller' => 'app\controller\ap\ServiceController',
        'GET' => [
            'method_name' => 'edit'
        ],
        'POST' => [
            'method_name' => 'update'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'delete_service' => [
        'controller' => 'app\controller\ap\ServiceController',
        'GET' => [
            'method_name' => ''
        ],
        'POST' => [
            'method_name' => 'delete'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],

    'blog' => [
        'controller' => 'app\controller\ap\BlogController',
        'GET' => [
            'method_name' => 'lists'
        ],
        'POST' => [
            'method_name' => ''
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'add_blog' => [
        'controller' => 'app\controller\ap\BlogController',
        'GET' => [
            'method_name' => 'create'
        ],
        'POST' => [
            'method_name' => 'store'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'edit_blog' => [
        'controller' => 'app\controller\ap\BlogController',
        'GET' => [
            'method_name' => 'edit'
        ],
        'POST' => [
            'method_name' => 'update'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'delete_blog' => [
        'controller' => 'app\controller\ap\BlogController',
        'GET' => [
            'method_name' => ''
        ],
        'POST' => [
            'method_name' => 'delete'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],

    'course' => [
        'controller' => 'app\controller\ap\CourseController',
        'GET' => [
            'method_name' => 'list'
        ],
        'POST' => [
            'method_name' => ''
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'add_course' => [
        'controller' => 'app\controller\ap\CourseController',
        'GET' => [
            'method_name' => 'add'
        ],
        'POST' => [
            'method_name' => 'store'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'edit_course' => [
        'controller' => 'app\controller\ap\CourseController',
        'GET' => [
            'method_name' => 'edit'
        ],
        'POST' => [
            'method_name' => 'update'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'delete_course' => [
        'controller' => 'app\controller\ap\CourseController',
        'GET' => [
            'method_name' => ''
        ],
        'POST' => [
            'method_name' => 'delete'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],


    'education' => [
        'controller' => 'app\controller\ap\EducationController',
        'GET' => [
            'method_name' => 'list'
        ],
        'POST' => [
            'method_name' => ''
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'add_education' => [
        'controller' => 'app\controller\ap\EducationController',
        'GET' => [
            'method_name' => 'add'
        ],
        'POST' => [
            'method_name' => 'store'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'edit_education' => [
        'controller' => 'app\controller\ap\EducationController',
        'GET' => [
            'method_name' => 'edit'
        ],
        'POST' => [
            'method_name' => 'update'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'delete_education' => [
        'controller' => 'app\controller\ap\EducationController',
        'GET' => [
            'method_name' => ''
        ],
        'POST' => [
            'method_name' => 'delete'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],


    'success' => [
        'controller' => 'app\controller\ap\SuccessController',
        'GET' => [
            'method_name' => 'list'
        ],
        'POST' => [
            'method_name' => ''
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'add_success' => [
        'controller' => 'app\controller\ap\SuccessController',
        'GET' => [
            'method_name' => 'add'
        ],
        'POST' => [
            'method_name' => 'store'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'edit_success' => [
        'controller' => 'app\controller\ap\SuccessController',
        'GET' => [
            'method_name' => 'edit'
        ],
        'POST' => [
            'method_name' => 'update'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'delete_success' => [
        'controller' => 'app\controller\ap\SuccessController',
        'GET' => [
            'method_name' => ''
        ],
        'POST' => [
            'method_name' => 'delete'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],

    'messages' => [
        'controller' => 'app\controller\ap\MessagesController',
        'GET' => [
            'method_name' => 'list'
        ],
        'POST' => [
            'method_name' => ''
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'delete_message' => [
        'controller' => 'app\controller\ap\MessagesController',
        'GET' => [
            'method_name' => ''
        ],
        'POST' => [
            'method_name' => 'delete_message'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],

    'users' => [
        'controller' => 'app\controller\ap\UserController',
        'GET' => [
            'method_name' => 'listUsers'
        ],
        'POST' => [
            'method_name' => 'about'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'add_user' => [
        'controller' => 'app\controller\ap\UserController',
        'GET' => [
            'method_name' => 'add_user'
        ],
        'POST' => [
            'method_name' => 'create_user'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'delete_user' => [
        'controller' => 'app\controller\ap\UserController',
        'POST' => [
            'method_name' => 'delete_user'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'edit_user' => [
        'controller' => 'app\controller\ap\UserController',
        'GET' => [
            'method_name' => 'edit_user'
        ],
        'POST' => [
            'method_name' => 'update_user'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
    'logout' => [
        'controller' => 'app\controller\ap\UserController',
        'GET' => [
            'method_name' => 'logout'
        ],
        'middleware' => 'app\middlewares\AdminMiddleware',
        'middleware_method' => 'index',
    ],
];

$api_routes = [
    '/admin' => [
        'controller' => 'app\controller\HomeController',
        'GET' => [
            'method_name' => 'index'
        ],
        'POST' => [
            'method_name' => 'about'
        ],
        'middleware' => 'app\middlewares\HomeMidlleware',
        'middleware_method' => 'index',
        'api' => false
    ]
];