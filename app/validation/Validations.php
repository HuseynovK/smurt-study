<?php

namespace app\validation;

use app\locale\LocalizationForErrors;

class Validations extends LocalizationForErrors implements ValidationMehtods
{
    public static function validate($request, $validation)
    {
        $error = true;
        foreach ($validation as $key => $value) {
//            var_dump($value);
//            var_dump($key);
//            die();
//            $tap = true;
            if (array_key_exists('nullable', $validation[$key])) {
                if ($validation[$key]['nullable']) {
                    if (array_key_exists($key, $request)) {
                        if (!$request[$key]) {
                            return [
                                'code' => 2
                            ];
                        }
                    } else {
                        return [
                            'code' => 2
                        ];
                    }
                }
            }
//            if ($tap) {
            foreach ($value as $k => $v) {

                if (isset($request[$key])) {
//                    var_dump($k);
//                    die();
                    switch ($k) {
                        case "confirm":
                            if ($v) {
                                if (array_key_exists('confirm_key', $value)) {
                                    $res = self::Confirm_Pass($request[$key], $request, $key, $value['confirm_key']);
                                } else {
                                    $res = self::Confirm_Pass($request[$key], $request, $key);
                                }
                                if (!$res['code']) {
                                    $error = true;
                                    return $res;
                                }
                                $error = false;
                            }
                            break;
                        case "required":
                            if ($v) {
                                $res = self::Required($request[$key], $key);
                                if (!$res['code']) {
                                    $error = true;
                                    return $res;
                                }
                                $error = false;
                            }
                            break;
                        case "min":
                            $res = self::minWord($request[$key], $v, $key);
                            if (!$res['code']) {
                                $error = true;
                                return $res;
                            }
                            $error = false;
                            break;
                        case "max":
                            $res = self::maxWord($request[$key], $v, $key);
                            if (!$res['code']) {
                                $error = true;
                                return $res;
                            }
                            $error = false;
                            break;
                        case "email":
                            if ($v) {
                                $res = self::IsEmail($request[$key], $key);
                                if (!$res['code']) {
                                    $error = true;
                                    return $res;
                                }
                                $error = false;
                            }
                            break;
                        case "min_size":
                            $res = self::ImageMinSize($request[$key], $v, $key);
                            if (!$res['code']) {
                                $error = true;
                                return $res;
                            }
                            $error = false;
                            break;
                        case "max_size":
                            $res = self::ImageMaxSize($request[$key], $v, $key);
                            if (!$res['code']) {
                                $error = true;
                                return $res;
                            }
                            $error = false;
                            break;
                        case "mime":
                            $res = self::ImageMimes($request[$key], $v, $key);
                            if (!$res['code']) {
                                $error = true;
                                return $res;
                            }
                            $error = false;
                            break;
                        case "type":
                            $res = self::Checktype($request[$key], $value['type'], $key);
                            if (!$res['code']) {
                                $error = true;
                                return $res;
                            }
                            $error = false;
                            break;
                        default:
//                        echo "Your favorite color is neither red, blue, nor green!";
                    }
                } else {

//                    var_dump();
                    $var = sprintf(LocalizationForErrors::getLocal($_SESSION['lang'], 'not_isset'), $key);

                    return [
                        'code' => false,
                        'message' => $var,
                    ];
                }
            }
//            }
        }

        if (!$error) {
            return [
                'code' => true
            ];
        }
    }

    public static function validateFile($request, $validation)
    {
        foreach ($validation as $key => $value) {
            var_dump($value);
            foreach ($value as $k => $v) {
                switch ($key) {
                    case "required":
                        echo "Your favorite color is red!";
                        break;
                    case "min":
                        echo "Your favorite color is blue!";
                        break;
                    case "max":
                        echo "Your favorite color is green!";
                        break;
                    default:
                        echo "Your favorite color is neither red, blue, nor green!";
                }
            }
        }
    }

    public static function Required($request, $key)
    {
        if (empty($request)) {
            return [
                'code' => false,
                'message' => sprintf(LocalizationForErrors::getLocal($_SESSION['lang'], 'required'), $key)
            ];
        } else {
            return [
                'code' => true
            ];
        }
    }

    public static function minWord($value, $check = 0, $key = false)
    {
        if (strlen("$value") >= $check) {
            return [
                'code' => true
            ];
        } else {
            return [
                'code' => false,
                'message' => sprintf(LocalizationForErrors::getLocal($_SESSION['lang'], 'min'), $key, $check)
            ];
        }
    }

    public static function maxWord($value, $check = 0, $key = false)
    {
        if (strlen("$value") <= $check) {
            return [
                'code' => true
            ];
        } else {
            return [
                'code' => false,
                'message' => sprintf(LocalizationForErrors::getLocal($_SESSION['lang'], 'max'), $key, $check)
            ];
        }
    }

    public static function ImageMinSize($image, $check, $key)
    {
        try {
            if (($image['size'] / 1024) >= $check) {
//                var_dump("Min Check");
                return [
                    'code' => true
                ];
            } else {
                return [
                    'code' => false,
                    'message' => sprintf(LocalizationForErrors::getLocal($_SESSION['lang'], 'min'), $key, $check)
                ];
            }
        } catch (\Exception $exception) {
            var_dump($exception);
        }
    }

    public static function ImageMaxSize($image, $check, $key)
    {
        try {
            if (($image['size'] / 1024) <= $check) {
//                var_dump("Max Check");
                return [
                    'code' => true
                ];
            } else {
                return [
                    'code' => false,
                    'message' => sprintf(LocalizationForErrors::getLocal($_SESSION['lang'], 'max'), $key, $check)
                ];
            }
        } catch (\Exception $exception) {
            var_dump($exception);
        }
    }

    public static function ImageMimes($image, $image_types, $key)
    {
        $path = $image['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        if (!in_array($ext, $image_types)) {
            return [
                'code' => false,
                'message' => sprintf(LocalizationForErrors::getLocal($_SESSION['lang'], 'image_ext'), $key)
            ];
        } else {
            return [
                'code' => true
            ];
        }
    }

    public static function IsEmail($value, $key)
    {
        if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return [
                'code' => true
            ];
        } else {
            return [
                'code' => false,
                'message' => sprintf(LocalizationForErrors::getLocal($_SESSION['lang'], 'email'), $key)
            ];
        }
    }

    public static function Checktype($value, $type, $key)
    {
        if ($type == "integer") {
            if (is_numeric($value)) {

//                var_dump(true==1);
//                die();
                return [
                    'code' => true
                ];
            }
        }
        return [
            'code' => false,
            'w' => $type,
            'message' => sprintf(LocalizationForErrors::getLocal('az', 'type'), $key)
        ];
    }

    public static function Confirm_Pass($value, $request, $key, $c_key = "confirm_password")
    {
        if (array_key_exists($c_key, $request)) {
            if (!$request[$c_key]) {
                return [
                    'code' => false,
                    'message' => sprintf(LocalizationForErrors::getLocal('az', 'type'), $key)
                ];
            } else {
                if ($request[$key] == $request[$c_key]) {
                    return [
                        'code' => true
                    ];
                } else {
                    return [
                        'code' => false,
                        'message' => sprintf(LocalizationForErrors::getLocal('az', 'confirm_p'), $c_key)
                    ];
                }
            }
        } else {
            return [
                'code' => false,
                'message' => sprintf(LocalizationForErrors::getLocal('az', 'type'), $key)
            ];
        }
    }
}