<?php

namespace app\validation;

interface ValidationMehtods
{
    public static function Required($request, $key);

    public static function IsEmail($value, $key);

    public static function minWord($value, $check = 0, $key = false);

    public static function maxWord($value, $check = 0, $key = false);

    public static function ImageMinSize($image, $check, $key);

    public static function ImageMaxSize($image, $check, $key);

    public static function ImageMimes($image, $image_type, $key);

    public static function Checktype($value, $type, $key);

    public static function Confirm_Pass($value, $request, $key, $c_key = "confirm_password");
}