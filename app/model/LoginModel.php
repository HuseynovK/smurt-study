<?php

namespace app\model;

use  app\web\db;

class LoginModel
{
    public static function LoginUser()
    {
        $con = (db::connect())
            ->prepare("SELECT * FROM `users` where email=:email limit 1");
        $con->execute([
            'email' => $_POST['email']
        ]);

        $d = $con->fetch(\PDO::FETCH_ASSOC);

        if (!$d) {
            return false;
        }
        // var_dump(encrypt('1'));
        // die();
        
// var_dump(decrypt("NedUlko2fePCAlX+deNA0lB4F5dlsTLsAyzNJc8jVn7rszGVhwnm0OayWeOZ5IWPKeCtrp3vU/8pxSWTI2gOjA==wKZfoCA+Bt6Djq0YkZ39Pf0BkGvarruJ8SGv3DRyf3d7BXNXFJWCCFDmK0pnZgq4ubLl43gFFzugd3Abt5qhgA=="));
// var_dump(decrypt($d['password']));
// die();
        if (decrypt($d['password']) == $_POST['password']) {
            $_SESSION['id'] = $d['id'];
            $_SESSION['email'] = $d['email'];
            $_SESSION['token'] = $d['token'];
            return $d;
        } else {
            return false;
        }
    }
}