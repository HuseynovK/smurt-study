<?php

namespace app\controller;

use app\model\LoginModel;
use app\validation\Validations;
use app\web\db;

class AuthController extends Controller
{
    public function login()
    {
        if (isset($_SESSION['token'])) {
            if ($_SESSION['token']) {
                $con = (db::connect())
                    ->prepare("SELECT * FROM `users` where token=:token limit 1");
                $con->execute([
                    'token' => $_SESSION['token']
                ]);

                $d = $con->fetch(\PDO::FETCH_ASSOC);

                if ($d) {
                    header('Location: ' . site_url_admin());
                }
            }
        }

        view('auth/login');
    }

    public function signin()
    {

        $result = Validations::validate($_POST, [
            'email' => ['required' => true,'email' => true],
            'password' => ['required' => true],
        ]);
        if ($result["code"] == 0) {
            redirectBack([
                "error" => $result['message'],
                "code" => -1
            ]);
        }

        $res = LoginModel::LoginUser();

        if ($res) {
            header('Location: ' . site_url_admin());
        } else {
            redirectBack([
                "error" => "Email or Password Incorrect",
                "code" => -1
            ]);
        }
    }
}
