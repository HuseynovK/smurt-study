<?php

namespace app\controller;

use app\model\Jugaad;
use app\model\LoginModel;
use app\validation\Validations;

class HomeController extends Controller
{
    public function index()
    {
//        var_dump("Kamil");
//        echo $_SERVER['DOCUMENT_ROOT'];
//        $root = $_SERVER['DOCUMENT_ROOT'] . "\\views\home.php";
//        require_once($root);
        $data = [
            "name" => ['Kamil', 'Huseynov'],
            "page" => 'home'
        ];

        view('layouts/head', $data);
    }

    public function c_post()
    {
        var_dump($_POST);
    }

    public function add_contact_message()
    {
//        var_dump($_POST);
//        die();
        $data = [
            "page" => 'contact',
            "error" => 0
        ];
//        var_dump(Jugaad::InsertMessage(["email" => $_POST['email'], "message" => $_POST['message']]));
//        die();
        if (Jugaad::InsertMessage(["email" => $_POST['email'], "message" => $_POST['message']])) {
            view('layouts/head', $data);
        } else {
            $data['error'] = 1;
            view('layouts/head', $data);
        }
    }

    public function about()
    {
        $data = [
            "name" => ['Kamil', 'Huseynov'],
            "page" => 'about'
        ];

        view('layouts/head', $data);
    }

    public function contact()
    {
        $data = [
            "name" => ['Kamil', 'Huseynov'],
            "page" => 'contact'
        ];
        view('layouts/head', $data);
    }

    public function services()
    {
        $data = [
            "name" => ['Kamil', 'Huseynov'],
            "page" => 'services'
        ];
        var_dump("Services");
    }

    public function login()
    {
        $data = [
            "name" => 'Login'
        ];

        view('auth/login', $data);
    }

    public function api_test()
    {
//        var_dump("Kamil");
        echo json_encode([
            'i' => "K"
        ]);
//        return json_encode("Kamil");
    }
}
