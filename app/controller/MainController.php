<?php


namespace app\controller;


use app\validation\Validations;
use app\web\db;

class MainController extends Controller
{
    public function index()
    {
        $query = "SELECT * FROM sm_education order by education_id desc";
        $con = (db::connect())->prepare($query);
        $con->execute();
        $data['education'] = $con->fetchAll();

        $query = "SELECT * FROM sm_country order by country_id desc limit 8";
        $con = (db::connect())->prepare($query);
        $con->execute();
        $data['country'] = $con->fetchAll();

        $query = "SELECT * FROM sm_service order by service_id desc";
        $con = (db::connect())->prepare($query);
        $con->execute();
        $data['service'] = $con->fetchAll();

        $query = "SELECT * FROM sm_blog order by blog_id desc limit 3";
        $con = (db::connect())->prepare($query);
        $con->execute();
        $data['blog'] = $con->fetchAll();


        $query = "SELECT * FROM sm_success order by success_id desc limit 3";
        $con = (db::connect())->prepare($query);
        $con->execute();
        $data['success'] = $con->fetchAll();;

        $query = "SELECT * FROM sm_course order by course_id desc";
        $con = (db::connect())->prepare($query);
        $con->execute();
        $data['course'] = $con->fetchAll();;


        view('home', $data);
    }

    public function blog_detail($id)
    {
        if ($id) {
            $query = "SELECT * FROM sm_blog where blog_id=:blog_id limit 1";
            $con = (db::connect())->prepare($query);
            $con->execute([
                'blog_id' => $id
            ]);

            $users = $con->fetch();
//            var_dump($users);
//            die();
            if ($users) {
                $data['blog'] = $users;

                $query = "SELECT * FROM sm_blog order by blog_id desc limit 3";
                $con = (db::connect())->prepare($query);
                $con->execute();

                $users = $con->fetchAll();
                $data['blogs'] = $users;
                view('blog-detail', $data);
            }
        }
    }

    public function blog()
    {
        $result = Validations::validate($_GET, [
            'page' => ['nullable' => true, 'type' => 'integer']
        ]);

        // $perpage = 1;

        // $where = "limit " . $perpage;

        // if ($result['code'] == 1) {
        //     if ($_GET['page'] > 1) {
        //         $where = "limit " . (($perpage * ($_GET['page'] - 1)) . ', ' . $perpage);
        //     }
        // }
        $where = ''; 

        $query = "SELECT * FROM sm_blog order by blog_id desc " . $where;
        $con = (db::connect())->prepare($query);
        $con->execute();

        $users = $con->fetchAll();
        $data['blog'] = $users;


        view('blog', $data);
    }

    public function country()
    {
        $query = "SELECT * FROM sm_country order by country_id desc";
        $con = (db::connect())->prepare($query);
        $con->execute();
        $data['country'] = $con->fetchAll();

        view('country',$data);
    }

    public function country_detail($id)
    {
        if($id){

            $query = "SELECT * FROM sm_country WHERE country_id=:country_id limit 1";
            $con = (db::connect())->prepare($query);
            $con->execute([
                'country_id' => $id
            ]);
            $data['country'] = $con->fetch();

            view('country-detail',$data);
        }
    }
    public function service_detail($id)
    {
        if($id){

            $query = "SELECT * FROM sm_service WHERE service_id=:service_id limit 1";
            $con = (db::connect())->prepare($query);
            $con->execute([
                'service_id' => $id
            ]);
            $data['service'] = $con->fetch();

            view('service-detail',$data);
        }
    }
    public function success()
    {
        
        $query = "SELECT * FROM sm_success order by success_id desc";
        $con = (db::connect())->prepare($query);
        $con->execute();
        $data['success'] = $con->fetchAll();

        view('success',$data);
    }

    public function course_detail($id)
    {
        if($id){

            $query = "SELECT * FROM sm_course WHERE course_id=:course_id limit 1";
            $con = (db::connect())->prepare($query);
            $con->execute([
                'course_id' => $id
            ]);
            $data['course'] = $con->fetch();

            view('course-detail',$data);
        }
    }

    public function message()
    {
        $result = Validations::validate($_POST, [
            'name' => ['required' => true],
            'phone' => ['required' => true],
            'surname' => ['required' => true],
            'service' => ['required' => true],
            'message' => ['required' => true],
        ]);

        if ($result['code'] == true) {

            $con = (db::connect())->prepare("INSERT INTO sm_message SET
            message_name=:message_name,
            message_surname=:message_surname,
            message_phone=:message_phone,
            message_type=:message_type,
            message_context=:message_context
            ");

        $up = $con->execute([
            "message_name" => $_POST['name'],
            "message_surname" => $_POST['surname'],
            "message_phone" => $_POST['phone'],
            "message_type" => $_POST['service'],
            "message_context" => $_POST['message'],
        ]);

        if (!$up) {
            redirectBack([
                "error" => "Something went wrong! 3",
                "code" => -1
            ]);
        } else {
            $_SESSION["back"]['code'] = 1;
            $ur = site_url("");
            header("Location:" . $ur);
            }
        } 


        else {
            redirectBack([
                "error" => $result['message'],
                "code" => -1
            ]);
        }
    }
   
}