<?php

namespace app\controller\ap;

use app\validation\Validations;
use  app\web\db;

class UserController
{
    public function listUsers()
    {
        $result = Validations::validate($_GET, [
            'page' => ['nullable' => true, 'type' => 'integer']
        ]);

        global $perpage;

        $where = "limit " . $perpage;
//        var_dump("Result : " . $result['code']);
//        var_dump($result);
        if ($result['code'] == 1) {
            if ($_GET['page'] > 1) {
                $where = "limit " . (($perpage * ($_GET['page'] - 1)) . ', ' . $perpage);
            }
        }

        $query = "SELECT * FROM users order by id " . $where;
        $con = (db::connect())->prepare($query);
        $con->execute();

        $users = $con->fetchAll();
//        var_dump($users);
//        die();

        $data['users'] = $users;

        $query = "SELECT count(id) as say FROM users";
        $con = (db::connect())->prepare($query);
        $con->execute();

        $say = $con->fetch(\PDO::FETCH_ASSOC)['say'];

        $data['say'] = $say;

        view('admin/users/list', $data);
    }

    public function add_user()
    {
        view('admin/users/add');
    }

    public function logout()
    {
        unset($_SESSION['token']);

        header("Location: " . site_url("login"), true);
    }

    public function edit_user()
    {
//        var_dump($_GET);
//        die();
        $result = Validations::validate($_GET, [
            'uid' => ['required' => true, 'type' => 'integer']
        ]);

        if ($result['code'] != true) {
//            var_dump($result);
//            die("KA");
            redirectBack([
                "error" => "Not found!",
                "code" => -1
            ]);
        }

        $query = "SELECT * FROM users where id=:id limit 1";
        $con = (db::connect())->prepare($query);

        $con->execute([
            "id" => $_GET["uid"]
        ]);

        $one = $con->fetch(\PDO::FETCH_ASSOC);
//        var_dump($one);
//        die();
//        session_start();
        if (!$one) {
//            var_dump($result);
//            die("KA");
            redirectBack([
                "error" => "User Not found!",
                "code" => -1
            ]);
        }

        view('admin/users/edit', $one);
    }

    public function delete_user($id)
    {
        if (is_numeric($id)) {
            $select = (db::connect())->prepare("SELECT id FROM users WHERE id=:id");

            $select->execute(array(
                'id' => $id
            ));

            $up2 = $select->fetch(\PDO::FETCH_ASSOC);
            if (!$up2) {
                redirectBack([
                    "error" => "User not found!",
                    "code" => -1
                ]);
            }

            $delete = (db::connect())->prepare("DELETE FROM `users` WHERE id=:id");

            $up = $delete->execute(array(
                'id' => $id
            ));

            if (!$up) {
                redirectBack([
                    "error" => "Something went wrong!",
                    "code" => -1
                ]);
            } else {
                redirectBack([
                    "code" => 2
                ]);
            }

        } else {
            redirectBack([
                "error" => "ID Wrong type",
                "code" => -1
            ]);
        }
    }

    public function create_user()
    {
        $result = Validations::validate($_POST, [
            'fullname' => ['required' => true, 'min' => '4'],
            'email' => ['required' => true, 'email' => true],
            'password' => ['required' => true, 'min' => 3, 'confirm' => true],
            'confirm_password' => ['required' => true, 'min' => 3]
        ]);

        if ($result['code'] == true) {

            $con = (db::connect())->prepare("INSERT INTO users SET
                     fullname=:fullname,
                     email=:email,
                     password=:password,
                     token=:token
                    ");

            $pass = encrypt($_POST['password']);
            $up = $con->execute([
                "fullname" => $_POST['fullname'],
                "email" => $_POST['email'],
                "password" => $pass,
                "token" => encrypt($_POST['email'] . "..." . encrypt($pass))
            ]);

            if (!$up) {
                redirectBack([
                    "error" => "Something went wrong!",
                    "code" => -1
                ]);
            } else {
                $_SESSION["back"]['code'] = 1;
                $ur = site_url_admin("users");
                header("Location:" . $ur);
            }
        } else {
            redirectBack([
                "error" => $result['message'],
                "code" => -1
            ]);
        }
    }

    public function update_user()
    {
        $result3 = Validations::validate($_GET, [
            'uid' => ['required' => true, 'type' => 'integer']
        ]);

        if ($result3['code'] != true) {
            redirectBack([
                "error" => "Not found!",
                "code" => -1
            ]);
        }

        $query = "SELECT * FROM users where id=:id";
        $con = (db::connect())->prepare($query);
        $con->execute([
            "id" => $_GET['uid']
        ]);

        $user = $con->fetch(\PDO::FETCH_ASSOC);

        if (!$user) {
            redirectBack([
                "error" => "User Tapilmadi",
                "code" => -1
            ]);
        }

        $result = Validations::validate($_POST, [
            'fullname' => ['required' => true, 'min' => '4'],
            'email' => ['required' => true, 'email' => true],
            'password' => ['nullable' => true, 'min' => 3, 'confirm' => true],
            'confirm_password' => ['nullable' => true, 'min' => 3]
        ]);

        if ($result['code'] == true || $result['code'] == 2) {

            $con = (db::connect())->prepare("UPDATE users SET
                     fullname=:fullname,
                     email=:email,
                     password=:password,
                     token=:token
                     where id=:id
                    ");

            $pass = $_POST['password'] ? ($_POST['password'] != decrypt($user['password']) ? encrypt($_POST['password']) : $user['password']) : $user['password'];

            $token = $user['token'];

            if ($_POST['password'] || $_POST['email'] != $user['email']) {
                $token = encrypt($_POST['email'] . "..." . encrypt($pass));
            }

            $up = $con->execute([
                "fullname" => $_POST['fullname'],
                "email" => $_POST['email'],
                "password" => $pass,
                "token" => $token,
                "id" => $_GET['uid']
            ]);

            if (!$up) {
                var_dump($con->errorInfo());
                die();
                redirectBack([
                    "error" => "Something went wrong!",
                    "code" => -1
                ]);
            } else {
                $_SESSION["back"]['code'] = 1;
                $ur = site_url_admin("users");
                header("Location: " . $ur);
            }
        } else {
            redirectBack([
                "error" => $result['message'],
                "code" => -1
            ]);
        }
    }
}