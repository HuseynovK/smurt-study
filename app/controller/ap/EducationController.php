<?php

namespace app\controller\ap;

use app\controller\Controller;
use app\validation\Validations;
use app\web\db;

class educationController extends Controller
{
    public function list()
    {
        $result = Validations::validate($_GET, [
            'page' => ['nullable' => true, 'type' => 'integer']
        ]);

        global $perpage;

        $where = "limit " . $perpage;

        if ($result['code'] == 1) {
            if ($_GET['page'] > 1) {
                $where = "limit " . (($perpage * ($_GET['page'] - 1)) . ', ' . $perpage);
            }
        }

        $query = "SELECT * FROM sm_education order by education_id desc " . $where;
        $con = (db::connect())->prepare($query);
        $con->execute();

        $education = $con->fetchAll();

        $data['education'] = $education;

        view('admin/education/list', $data);
    }

    public function add()
    {
        view('admin/education/add');
    }

    public function edit()
    {
        $result = Validations::validate($_GET, [
            'uid' => ['required' => true, 'type' => 'integer']
        ]);

        if ($result['code'] != true) {
            redirectBack([
                "error" => "Not found!",
                "code" => -1
            ]);
        }

        $query = "SELECT * FROM sm_education where education_id=:education_id limit 1";
        $con = (db::connect())->prepare($query);

        $con->execute([
            "education_id" => $_GET["uid"]
        ]);

        $one = $con->fetch(\PDO::FETCH_ASSOC);

        if (!$one) {
            redirectBack([
                "error" => "User Not found!",
                "code" => -1
            ]);
        }

        view('admin/education/edit', $one);
    }

    public function delete($id)
    {
        if (is_numeric($id)) {
            $select = (db::connect())->prepare("SELECT education_id,education_image FROM sm_education WHERE education_id=:education_id");

            $select->execute(array(
                'education_id' => $id
            ));

            $up2 = $select->fetch(\PDO::FETCH_ASSOC);
            if (!$up2) {
                redirectBack([
                    "error" => "User not found!",
                    "code" => -1
                ]);
            }
            if (file_exists(assetImage('asset/uploads/Education/' . $up2['education_image']))) {
                unlink(assetImage('asset/uploads/Education/' . $up2['education_image']));
            }

            $delete = (db::connect())->prepare("DELETE FROM `sm_education` WHERE education_id=:education_id");

            $up = $delete->execute(array(
                'education_id' => $id
            ));

            if (!$up) {
                redirectBack([
                    "error" => "Something went wrong!",
                    "code" => -1
                ]);
            } else {
                redirectBack([
                    "code" => 2
                ]);
            }
        } else {
            redirectBack([
                "error" => "ID Wrong type",
                "code" => -1
            ]);
        }
    }

    public function store()
    {
        $result2 = Validations::validate($_FILES, [
            'image' => ['required' => true, 'mime' => ['jpg', 'jpeg', 'png','svg']],
        ]);
        if ($result2['code'] != true) {
            redirectBack([
                "error" => $result2['message'],
                "code" => -1
            ]);
        }

        $result = Validations::validate($_POST, [
            'name' => ['required' => true],
        ]);

        if ($result['code'] == true) {

            $image = null;
            $path = assetImage('asset/uploads/Education/');
            $file = $_FILES['image'];
            $file_ext = explode('.', $file['name']);
            $file_ext = strtolower(end($file_ext));
            $file_full_name = uniqid() . '.' . $file_ext;
            $file_saved = $path . $file_full_name;

            $mm = move_uploaded_file($file['tmp_name'], $file_saved);

            if ($file_full_name) {
                $con = (db::connect())->prepare("INSERT INTO sm_education SET
                    education_image=:education_image,
                    education_name=:education_name
                    ");

                $up = $con->execute([
                    "education_image" => $file_full_name,
                    "education_name" => $_POST['name'],
                ]);

                if (!$up) {
                    redirectBack([
                        "error" => "Something went wrong! 3",
                        "code" => -1
                    ]);
                } else {
                    $_SESSION["back"]['code'] = 1;
                    $ur = site_url_admin("education");
                    header("Location:" . $ur);
                }
            } else {
                redirectBack([
                    "error" => "Something went wrong! 2",
                    "code" => -1
                ]);
            }
        } else {
            redirectBack([
                "error" => $result['message'],
                "code" => -1
            ]);
        }
    }

    public function update()
    {
        $result3 = Validations::validate($_GET, [
            'uid' => ['required' => true, 'type' => 'integer']
        ]);
        if ($result3['code'] != true) {
            redirectBack([
                "error" => "Not found!",
                "code" => -1
            ]);
        }

        $result = Validations::validate($_POST, [
            'name' => ['required' => true],
        ]);

        if ($result['code'] != true) {
            redirectBack([
                "error" => $result['message'],
                "code" => -1
            ]);
        }

        $query = "SELECT * FROM sm_education where education_id=:education_id";
        $con = (db::connect())->prepare($query);
        $con->execute([
            "education_id" => $_GET['uid']
        ]);

        $user = $con->fetch(\PDO::FETCH_ASSOC);

        if (!$user) {
            redirectBack([
                "error" => "User Tapilmadi",
                "code" => -1
            ]);
        }
        $image = $user['education_image'];
        
        if ($_FILES['image']['name']) {
            $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
            if (!in_array($ext, ['jpg', 'jpeg', 'png','svg'])) {
                redirectBack([
                    "error" => "Only jpg,Png,Jpeg extentions files accepting",
                    "code" => -1
                ]);
            } else {
                $path = assetImage('asset/uploads/Education/');
                if (file_exists($path . $image)) {
                    unlink($path . $image);
                }
                $image = null;
                $file = $_FILES['image'];
                $file_ext = explode('.', $file['name']);
                $file_ext = strtolower(end($file_ext));
                $file_full_name = uniqid() . '.' . $file_ext;
                $image = $file_full_name;

                $file_saved = $path . $file_full_name;

                $mm = move_uploaded_file($file['tmp_name'], $file_saved);
     
            }
        }


        $con = (db::connect())->prepare("UPDATE sm_education SET
                    education_image=:education_image,
                    education_name=:education_name
                    where education_id=:education_id
                    ");
        $up = $con->execute([
            "education_image" => $image,
            "education_name" => $_POST['name'],
            "education_id" => $user['education_id']
        ]);
    
        if (!$up) {
        
            redirectBack([
                "error" => "Something went wrong!",
                "code" => -1
            ]);
        } else {
            $_SESSION["back"]['code'] = 1;
            $ur = site_url_admin("education");
            header("Location: " . $ur);
        }
    }
}
