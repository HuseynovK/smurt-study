<?php

namespace app\controller\ap;

use app\controller\Controller;
use app\validation\Validations;
use app\web\db;



class BlogController extends Controller
{
    public function lists()
    {
        $result = Validations::validate($_GET, [
            'page' => ['nullable' => true, 'type' => 'integer']
        ]);

        global $perpage;

        $where = "limit " . $perpage;

        if ($result['code'] == 1) {
            if ($_GET['page'] > 1) {
                $where = "limit " . (($perpage * ($_GET['page'] - 1)) . ', ' . $perpage);
            }
        }

        $query = "SELECT * FROM sm_blog order by blog_id desc " . $where;
        $con = (db::connect())->prepare($query);
        $con->execute();

        $users = $con->fetchAll();

        $data['blog'] = $users;

        view('admin/blog/list', $data);
    }

    public function create()
    {
        view('admin/blog/add');
    }

    public function edit()
    {
        $result = Validations::validate($_GET, [
            'uid' => ['required' => true, 'type' => 'integer']
        ]);

        if ($result['code'] != true) {
            redirectBack([
                "error" => "Not found!",
                "code" => -1
            ]);
        }

        $query = "SELECT * FROM sm_blog where blog_id=:blog_id limit 1";
        $con = (db::connect())->prepare($query);

        $con->execute([
            "blog_id" => $_GET["uid"]
        ]);

        $one = $con->fetch(\PDO::FETCH_ASSOC);

        if (!$one) {
            redirectBack([
                "error" => "User Not found!",
                "code" => -1
            ]);
        }

        view('admin/blog/edit', $one);
    }

    public function delete($id)
    {
        if (is_numeric($id)) {
            $select = (db::connect())->prepare("SELECT blog_id,blog_image,blog_author_image FROM sm_blog WHERE blog_id=:blog_id");

            $select->execute(array(
                'blog_id' => $id
            ));

            $up2 = $select->fetch(\PDO::FETCH_ASSOC);
            if (!$up2) {
                redirectBack([
                    "error" => "User not found!",
                    "code" => -1
                ]);
            }
            if (file_exists(assetImage('asset/uploads/Blog/' . $up2['blog_image']))) {
                unlink(assetImage('asset/uploads/Blog/' . $up2['blog_image']));
            }
            if (file_exists(assetImage('asset/uploads/Blog/' . $up2['blog_author_image']))) {
                unlink(assetImage('asset/uploads/Blog/' . $up2['blog_author_image']));
            }
            $delete = (db::connect())->prepare("DELETE FROM `sm_blog` WHERE blog_id=:blog_id");

            $up = $delete->execute(array(
                'blog_id' => $id
            ));

            if (!$up) {
                redirectBack([
                    "error" => "Something went wrong!",
                    "code" => -1
                ]);
            } else {
                redirectBack([
                    "code" => 2
                ]);
            }

        } else {
            redirectBack([
                "error" => "ID Wrong type",
                "code" => -1
            ]);
        }
    }

    public function store()
    {
        $result2 = Validations::validate($_FILES, [
            'image' => ['required' => true, 'mime' => ['jpg', 'jpeg', 'png']],
        ]);
        if ($result2['code'] != true) {
            redirectBack([
                "error" => $result2['message'],
                "code" => -1
            ]);
        }

        $result = Validations::validate($_POST, [
            'context' => ['required' => true],
            'author' => ['required' => true],
            'title' => ['required' => true],
        ]);

        if ($result['code'] == true) {

            $authorImage = null;
            $path = assetImage('asset/uploads/Blog/');
            $file = $_FILES['authorimage'];
            $file_ext = explode('.', $file['name']);
            $file_ext = strtolower(end($file_ext));
            $file_full_name = uniqid() . '.' . $file_ext;
            $authorImage = $file_full_name;
            $file_saved = $path . $file_full_name;
            $nn = move_uploaded_file($file['tmp_name'], $file_saved);
            

            $image = null;
            $path = assetImage('asset/uploads/Blog/');
            $file = $_FILES['image'];
            $file_ext = explode('.', $file['name']);
            $file_ext = strtolower(end($file_ext));
            $file_full_name = uniqid() . '.' . $file_ext;
            $file_saved = $path . $file_full_name;
            $mm = move_uploaded_file($file['tmp_name'], $file_saved);

            if ($mm && $nn) {
                $con = (db::connect())->prepare("INSERT INTO sm_blog SET
                    blog_image=:blog_image,
                    blog_author_image=:blog_author_image,
                    blog_context=:blog_context,
                    blog_title=:blog_title,
                    blog_author=:blog_author
                    ");

                $up = $con->execute([
                    "blog_image" => $file_full_name,
                    "blog_author_image" => $authorImage,
                    "blog_author" => $_POST['author'],
                    "blog_title" => $_POST['title'],
                    'blog_context' => $_POST['context']
                ]);

                if (!$up) {
                    redirectBack([
                        "error" => "Something went wrong! 3",
                        "code" => -1
                    ]);
                } else {
                    $_SESSION["back"]['code'] = 1;
                    $ur = site_url_admin("blog");
                    header("Location:" . $ur);
                }
            } else {
                redirectBack([
                    "error" => "Something went wrong! 2",
                    "code" => -1
                ]);
            }
        } else {
            redirectBack([
                "error" => $result['message'],
                "code" => -1
            ]);
        }
    }

    public function update()
    {
        $result3 = Validations::validate($_GET, [
            'uid' => ['required' => true, 'type' => 'integer']
        ]);
        if ($result3['code'] != true) {
            redirectBack([
                "error" => "Not found!",
                "code" => -1
            ]);
        }

        $result = Validations::validate($_POST, [
            'context' => ['required' => true],
            'title' => ['required' => true],
            'author' => ['required' => true],
        ]);

        if ($result['code'] != true) {
            redirectBack([
                "error" => $result['message'],
                "code" => -1
            ]);
        }

        $query = "SELECT * FROM sm_blog where blog_id=:blog_id";
        $con = (db::connect())->prepare($query);
        $con->execute([
            "blog_id" => $_GET['uid']
        ]);

        $user = $con->fetch(\PDO::FETCH_ASSOC);

        if (!$user) {
            redirectBack([
                "error" => "User Tapilmadi",
                "code" => -1
            ]);
        }
        $image = $user['blog_image'];
        $authorImage = $user['blog_author_image'];
        if ($_FILES['image']['name']) {
            $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
            if (!in_array($ext, ['jpg', 'jpeg', 'png'])) {
                redirectBack([
                    "error" => "Only jpg,Png,Jpeg extentions files accepting",
                    "code" => -1
                ]);
            } else {
                $path = assetImage('asset/uploads/Blog/');
                if (file_exists($path . $image)) {
                    unlink($path . $image);
                }
                $image = null;
                $file = $_FILES['image'];
                $file_ext = explode('.', $file['name']);
                $file_ext = strtolower(end($file_ext));
                $file_full_name = uniqid() . '.' . $file_ext;
                $image = $file_full_name;
                $file_saved = $path . $file_full_name;

                $mm = move_uploaded_file($file['tmp_name'], $file_saved);
            }
        }

        if ($_FILES['authorimage']['name']) {
            $ext = pathinfo($_FILES['authorimage']['name'], PATHINFO_EXTENSION);
            if (!in_array($ext, ['jpg', 'jpeg', 'png'])) {
                redirectBack([
                    "error" => "Only jpg,Png,Jpeg extentions files accepting",
                    "code" => -1
                ]);
            } else {
                $path = assetImage('asset/uploads/Blog/');
                if (file_exists($path . $authorImage)) {
                    unlink($path . $authorImage);
                }
                $authorImage = null;
                $file = $_FILES['authorimage'];
                $file_ext = explode('.', $file['name']);
                $file_ext = strtolower(end($file_ext));
                $file_full_name = uniqid() . '.' . $file_ext;
                $authorImage = $file_full_name;
                $file_saved = $path . $file_full_name;

                $mm = move_uploaded_file($file['tmp_name'], $file_saved);
            }
        }


        $con = (db::connect())->prepare("UPDATE sm_blog SET
                    blog_image=:blog_image,
                    blog_author_image=:blog_author_image,
                    blog_time=:blog_time,
                    blog_author=:blog_author,
                    blog_context=:blog_context,
                    blog_title=:blog_title
                     where blog_id=:blog_id
                    ");

        $up = $con->execute([
            "blog_image" => $image,
            "blog_author_image" => $authorImage,
            'blog_author' => $_POST['author'],
            'blog_context' => $_POST['context'],
            'blog_time' => $_POST['date'],
            'blog_title' => $_POST['title'],
            "blog_id" => $user['blog_id'],
        ]);
        if (!$up) {
            redirectBack([
                "error" => "Something went wrong!",
                "code" => -1
            ]);
        } else {
            $_SESSION["back"]['code'] = 1;
            $ur = site_url_admin("blog");
            header("Location: " . $ur);
        }
    }
}
