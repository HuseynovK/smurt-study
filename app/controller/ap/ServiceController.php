<?php

namespace app\controller\ap;

use app\controller\Controller;
use app\validation\Validations;
use app\web\db;

class ServiceController extends Controller
{
    public function list()
    {
        $result = Validations::validate($_GET, [
            'page' => ['nullable' => true, 'type' => 'integer']
        ]);

        global $perpage;

        $where = "limit " . $perpage;

        if ($result['code'] == 1) {
            if ($_GET['page'] > 1) {
                $where = "limit " . (($perpage * ($_GET['page'] - 1)) . ', ' . $perpage);
            }
        }

        $query = "SELECT * FROM sm_service order by service_id desc " . $where;
        $con = (db::connect())->prepare($query);
        $con->execute();

        $service = $con->fetchAll();

        $data['service'] = $service;

        view('admin/service/list', $data);
    }

    public function add()
    {
        view('admin/service/add');
    }

    public function edit()
    {
        $result = Validations::validate($_GET, [
            'uid' => ['required' => true, 'type' => 'integer']
        ]);

        if ($result['code'] != true) {
            redirectBack([
                "error" => "Not found!",
                "code" => -1
            ]);
        }

        $query = "SELECT * FROM sm_service where service_id=:service_id limit 1";
        $con = (db::connect())->prepare($query);

        $con->execute([
            "service_id" => $_GET["uid"]
        ]);

        $one = $con->fetch(\PDO::FETCH_ASSOC);

        if (!$one) {
            redirectBack([
                "error" => "User Not found!",
                "code" => -1
            ]);
        }

        view('admin/service/edit', $one);
    }

    public function delete($id)
    {
        if (is_numeric($id)) {
            $select = (db::connect())->prepare("SELECT service_id,service_image,service_single_image FROM sm_service WHERE service_id=:service_id");

            $select->execute(array(
                'service_id' => $id
            ));

            $up2 = $select->fetch(\PDO::FETCH_ASSOC);
            if (!$up2) {
                redirectBack([
                    "error" => "User not found!",
                    "code" => -1
                ]);
            }
            if (file_exists(assetImage('asset/uploads/Service/' . $up2['service_image']))) {
                unlink(assetImage('asset/uploads/Service/' . $up2['service_image']));
            }
            if (file_exists(assetImage('asset/uploads/Service/' . $up2['service__single_image']))) {
                unlink(assetImage('asset/uploads/Service/' . $up2['service_single_image']));
            }

            $delete = (db::connect())->prepare("DELETE FROM `sm_service` WHERE service_id=:service_id");

            $up = $delete->execute(array(
                'service_id' => $id
            ));

            if (!$up) {
                redirectBack([
                    "error" => "Something went wrong!",
                    "code" => -1
                ]);
            } else {
                redirectBack([
                    "code" => 2
                ]);
            }
        } else {
            redirectBack([
                "error" => "ID Wrong type",
                "code" => -1
            ]);
        }
    }

    public function store()
    {
        $result2 = Validations::validate($_FILES, [
            'image' => ['required' => true, 'mime' => ['jpg', 'jpeg', 'png','svg']],
            'single_image' => ['required' => true, 'mime' => ['jpg', 'jpeg', 'png','svg']]
        ]);
        if ($result2['code'] != true) {
            redirectBack([
                "error" => $result2['message'],
                "code" => -1
            ]);
        }

        $result = Validations::validate($_POST, [
            'name' => ['required' => true],
            'context' => ['required' => true]
        ]);

        if ($result['code'] == true) {
            $single_image = null;
            $path = assetImage('asset/uploads/Service/');
            $file = $_FILES['single_image'];
            $file_ext = explode('.', $file['name']);
            $file_ext = strtolower(end($file_ext));
            $file_full_name = uniqid() . '.' . $file_ext;
            $single_image = $file_full_name;
            $file_saved = $path . $file_full_name;
            $nn = move_uploaded_file($file['tmp_name'], $file_saved);
            
            $image = null;
            $path = assetImage('asset/uploads/Service/');
            $file = $_FILES['image'];
            $file_ext = explode('.', $file['name']);
            $file_ext = strtolower(end($file_ext));
            $file_full_name = uniqid() . '.' . $file_ext;
            $file_saved = $path . $file_full_name;

            $mm = move_uploaded_file($file['tmp_name'], $file_saved);

            if ($mm && $nn) {
                $con = (db::connect())->prepare("INSERT INTO sm_service SET
                    service_image=:service_image,
                    service_single_image=:service_single_image,
                    service_name=:service_name,
                    service_context=:service_context
                    ");

                $up = $con->execute([
                    "service_image" => $file_full_name,
                    "service_single_image" => $single_image,
                    "service_name" => $_POST['name'],
                    "service_context" => $_POST['context']
                ]);

                if (!$up) {
                    redirectBack([
                        "error" => "Something went wrong! 3",
                        "code" => -1
                    ]);
                } else {
                    $_SESSION["back"]['code'] = 1;
                    $ur = site_url_admin("service");
                    header("Location:" . $ur);
                }
            } else {
                redirectBack([
                    "error" => "Something went wrong! 2",
                    "code" => -1
                ]);
            }
        } else {
            redirectBack([
                "error" => $result['message'],
                "code" => -1
            ]);
        }
    }

    public function update()
    {
        $result3 = Validations::validate($_GET, [
            'uid' => ['required' => true, 'type' => 'integer']
        ]);
        if ($result3['code'] != true) {
            redirectBack([
                "error" => "Not found!",
                "code" => -1
            ]);
        }

        $result = Validations::validate($_POST, [
            'name' => ['required' => true],
            'context' => ['required' => true]
        ]);

        if ($result['code'] != true) {
            redirectBack([
                "error" => $result['message'],
                "code" => -1
            ]);
        }

        $query = "SELECT * FROM sm_service where service_id=:service_id";
        $con = (db::connect())->prepare($query);
        $con->execute([
            "service_id" => $_GET['uid']
        ]);

        $user = $con->fetch(\PDO::FETCH_ASSOC);

        if (!$user) {
            redirectBack([
                "error" => "User Tapilmadi",
                "code" => -1
            ]);
        }
        $image = $user['service_image'];
        $single_image = $user['service_single_image'];
        
        if ($_FILES['image']['name']) {
            $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
            if (!in_array($ext, ['jpg', 'jpeg', 'png','svg'])) {
                redirectBack([
                    "error" => "Only jpg,Png,Jpeg extentions files accepting",
                    "code" => -1
                ]);
            } else {
                $path = assetImage('asset/uploads/Service/');
                if (file_exists($path . $image)) {
                    unlink($path . $image);
                }
                $image = null;
                $file = $_FILES['image'];
                $file_ext = explode('.', $file['name']);
                $file_ext = strtolower(end($file_ext));
                $file_full_name = uniqid() . '.' . $file_ext;
                $image = $file_full_name;

                $file_saved = $path . $file_full_name;

                $mm = move_uploaded_file($file['tmp_name'], $file_saved);
     
            }
        }


        if ($_FILES['single_image']['name']) {
            $ext = pathinfo($_FILES['single_image']['name'], PATHINFO_EXTENSION);
            if (!in_array($ext, ['jpg', 'jpeg', 'png','svg'])) {
                redirectBack([
                    "error" => "Only jpg,Png,Jpeg extentions files accepting",
                    "code" => -1
                ]);
            } else {
                $path = assetImage('asset/uploads/Service/');
                if (file_exists($path . $single_image)) {
                    unlink($path . $single_image);
                }
                $single_image = null;
                $file = $_FILES['single_image'];
                $file_ext = explode('.', $file['name']);
                $file_ext = strtolower(end($file_ext));
                $file_full_name = uniqid() . '.' . $file_ext;
                $single_image = $file_full_name;
                $file_saved = $path . $file_full_name;

                $mm = move_uploaded_file($file['tmp_name'], $file_saved);
     
            }
        }

        $con = (db::connect())->prepare("UPDATE sm_service SET
                    service_image=:service_image,
                    service_single_image=:service_single_image,
                    service_time=:service_time,
                    service_name=:service_name,
                    service_context=:service_context
                    where service_id=:service_id
                    ");
        $up = $con->execute([
            "service_image" => $image,
            "service_single_image" => $single_image,
            "service_name" => $_POST['name'],
            "service_context" => $_POST['context'],
            'service_time' => $_POST['date'],
            "service_id" => $user['service_id']
        ]);
    
        if (!$up) {
        
            redirectBack([
                "error" => "Something went wrong!",
                "code" => -1
            ]);
        } else {
            $_SESSION["back"]['code'] = 1;
            $ur = site_url_admin("service");
            header("Location: " . $ur);
        }
    }
}
