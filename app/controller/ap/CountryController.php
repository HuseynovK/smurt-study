<?php

namespace app\controller\ap;

use app\controller\Controller;
use app\validation\Validations;
use app\web\db;

class CountryController extends Controller
{
    public function list()
    {
        $result = Validations::validate($_GET, [
            'page' => ['nullable' => true, 'type' => 'integer']
        ]);

        global $perpage;

        $where = "limit " . $perpage;

        if ($result['code'] == 1) {
            if ($_GET['page'] > 1) {
                $where = "limit " . (($perpage * ($_GET['page'] - 1)) . ', ' . $perpage);
            }
        }

        $query = "SELECT * FROM sm_country LEFT JOIN `users` ON users.id=sm_country.admin_id order by country_id desc " . $where;
        $con = (db::connect())->prepare($query);
        $con->execute();

        $country = $con->fetchAll();

        $data['country'] = $country;

        view('admin/country/list', $data);
    }

    public function add()
    {
        view('admin/country/add');
    }

    public function edit()
    {
        $result = Validations::validate($_GET, [
            'uid' => ['required' => true, 'type' => 'integer']
        ]);

        if ($result['code'] != true) {
            redirectBack([
                "error" => "Not found!",
                "code" => -1
            ]);
        }

        $query = "SELECT * FROM sm_country where country_id=:country_id limit 1";
        $con = (db::connect())->prepare($query);

        $con->execute([
            "country_id" => $_GET["uid"]
        ]);

        $one = $con->fetch(\PDO::FETCH_ASSOC);

        if (!$one) {
            redirectBack([
                "error" => "User Not found!",
                "code" => -1
            ]);
        }

        view('admin/country/edit', $one);
    }

    public function delete($id)
    {
        if (is_numeric($id)) {
            $select = (db::connect())->prepare("SELECT country_id,country_photo_path FROM sm_country WHERE country_id=:country_id");

            $select->execute(array(
                'country_id' => $id
            ));

            $up2 = $select->fetch(\PDO::FETCH_ASSOC);
            if (!$up2) {
                redirectBack([
                    "error" => "User not found!",
                    "code" => -1
                ]);
            }
            if (file_exists(assetImage('/asset/uploads/Country/' . $up2['country_photo_path']))) {
                unlink(assetImage('/asset/uploads/Country/' . $up2['country_photo_path']));
            }

            $delete = (db::connect())->prepare("DELETE FROM `sm_country` WHERE country_id=:country_id");

            $up = $delete->execute(array(
                'country_id' => $id
            ));

            if (!$up) {
                redirectBack([
                    "error" => "Something went wrong!",
                    "code" => -1
                ]);
            } else {
                redirectBack([
                    "code" => 2
                ]);
            }
        } else {
            redirectBack([
                "error" => "ID Wrong type",
                "code" => -1
            ]);
        }
    }

    public function store()
    {
        $result2 = Validations::validate($_FILES, [
            'image' => ['required' => true, 'mime' => ['jpg', 'jpeg', 'png']],
        ]);
        if ($result2['code'] != true) {
            redirectBack([
                "error" => $result2['message'],
                "code" => -1
            ]);
        }

        $result = Validations::validate($_POST, [
            'name' => ['required' => true],
            'context' => ['required' => true],
        ]);

        if ($result['code'] == true) {

            $image = null;

            $path = assetImage('asset/uploads/Country/');
            $file = $_FILES['image'];
            $file_ext = explode('.', $file['name']);
            $file_ext = strtolower(end($file_ext));
            $file_full_name = uniqid() . '.' . $file_ext;
            $file_saved = $path . $file_full_name;

            $mm = move_uploaded_file($file['tmp_name'], $file_saved);
            if ($file_full_name) {
                $con = (db::connect())->prepare("INSERT INTO sm_country SET
                    country_photo_path=:country_photo_path,
                    country_name=:country_name,
                    admin_id=:admin_id,
                    country_context=:country_context
                    ");

                $up = $con->execute([
                    "country_photo_path" => $file_full_name,
                    "admin_id" => $_SESSION['id'],
                    "country_name" => $_POST['name'],
                    'country_context' => $_POST['context'],
                ]);

                if (!$up) {
                    redirectBack([
                        "error" => "Something went wrong! 3",
                        "code" => -1
                    ]);
                } else {
                    $_SESSION["back"]['code'] = 1;
                    $ur = site_url_admin("country");
                    header("Location:" . $ur);
                }
            } else {
                redirectBack([
                    "error" => "Something went wrong! 2",
                    "code" => -1
                ]);
            }
        } else {
            redirectBack([
                "error" => $result['message'],
                "code" => -1
            ]);
        }
    }

    public function update()
    {
        $result3 = Validations::validate($_GET, [
            'uid' => ['required' => true, 'type' => 'integer']
        ]);
        if ($result3['code'] != true) {
            redirectBack([
                "error" => "Not found!",
                "code" => -1
            ]);
        }

        $result = Validations::validate($_POST, [
            'context' => ['required' => true],
            'name' => ['required' => true],
        ]);

        if ($result['code'] != true) {
            redirectBack([
                "error" => $result['message'],
                "code" => -1
            ]);
        }

        $query = "SELECT * FROM sm_country where country_id=:country_id";
        $con = (db::connect())->prepare($query);
        $con->execute([
            "country_id" => $_GET['uid']
        ]);

        $user = $con->fetch(\PDO::FETCH_ASSOC);

        if (!$user) {
            redirectBack([
                "error" => "User Tapilmadi",
                "code" => -1
            ]);
        }
        $image = $user['country_photo_path'];
        
        if ($_FILES['image']['name']) {
            $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
            if (!in_array($ext, ['jpg', 'jpeg', 'png'])) {
                redirectBack([
                    "error" => "Only jpg,Png,Jpeg extentions files accepting",
                    "code" => -1
                ]);
            } else {
                $path = assetImage('asset/uploads/Country/');
                if (file_exists($path . $image)) {
                    unlink($path . $image);
                }
                $image = null;
                $file = $_FILES['image'];
                $file_ext = explode('.', $file['name']);
                $file_ext = strtolower(end($file_ext));
                $file_full_name = uniqid() . '.' . $file_ext;
                $image = $file_full_name;

                $file_saved = $path . $file_full_name;

                $mm = move_uploaded_file($file['tmp_name'], $file_saved);
     
            }
        }


        $con = (db::connect())->prepare("UPDATE sm_country SET
                    country_photo_path=:country_photo_path,
                    admin_id=:admin_id,
                    country_created_at=:country_created_at,
                    country_context=:country_context,
                    country_name=:country_name
                    where country_id=:country_id
                    ");
        $up = $con->execute([
            "country_photo_path" => $image,
            'admin_id' =>$_SESSION['id'],
            'country_context' => $_POST['context'],
            "country_name" => $_POST['name'],
            'country_created_at' => $_POST['date'],
            "country_id" => $user['country_id']
        ]);
    
        if (!$up) {
        
            redirectBack([
                "error" => "Something went wrong!",
                "code" => -1
            ]);
        } else {
            $_SESSION["back"]['code'] = 1;
            $ur = site_url_admin("country");
            header("Location: " . $ur);
        }
    }
}
