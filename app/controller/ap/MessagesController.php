<?php

namespace app\controller\ap;

use app\controller\Controller;
use app\validation\Validations;
use app\web\db;

class MessagesController extends Controller
{
    public function list()
    {
        $result = Validations::validate($_GET, [
            'page' => ['nullable' => true, 'type' => 'integer']
        ]);

        global $perpage;

        $where = "limit " . $perpage;

        if ($result['code'] == 1) {
            if ($_GET['page'] > 1) {
                $where = "limit " . (($perpage * ($_GET['page'] - 1)) . ', ' . $perpage);
            }
        }

        $query = "SELECT * FROM sm_message order by message_id desc " . $where;
        $con = (db::connect())->prepare($query);
        $con->execute();

        $course = $con->fetchAll();

        $data['messages'] = $course;

        view('admin/message/list', $data);
    }


    public function delete_message($id)
    {
        if (is_numeric($id)) {
  

            $delete = (db::connect())->prepare("DELETE FROM `sm_message` WHERE message_id=:message_id");

            $up = $delete->execute(array(
                'message_id' => $id
            ));

            if (!$up) {
                redirectBack([
                    "error" => "Something went wrong!",
                    "code" => -1
                ]);
            } else {
                redirectBack([
                    "code" => 2
                ]);
            }
        } else {
            redirectBack([
                "error" => "ID Wrong type",
                "code" => -1
            ]);
        }
    }


}
