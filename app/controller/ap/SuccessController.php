<?php

namespace app\controller\ap;

use app\controller\Controller;
use app\validation\Validations;
use app\web\db;

class SuccessController extends Controller
{
    public function list()
    {
        $result = Validations::validate($_GET, [
            'page' => ['nullable' => true, 'type' => 'integer']
        ]);

        global $perpage;

        $where = "limit " . $perpage;

        if ($result['code'] == 1) {
            if ($_GET['page'] > 1) {
                $where = "limit " . (($perpage * ($_GET['page'] - 1)) . ', ' . $perpage);
            }
        }

        $query = "SELECT * FROM sm_success order by success_id desc " . $where;
        $con = (db::connect())->prepare($query);
        $con->execute();

        $success = $con->fetchAll();

        $data['success'] = $success;

        view('admin/success/list', $data);
    }

    public function add()
    {
        view('admin/success/add');
    }

    public function edit()
    {
        $result = Validations::validate($_GET, [
            'uid' => ['required' => true, 'type' => 'integer']
        ]);

        if ($result['code'] != true) {
            redirectBack([
                "error" => "Not found!",
                "code" => -1
            ]);
        }

        $query = "SELECT * FROM sm_success where success_id=:success_id limit 1";
        $con = (db::connect())->prepare($query);

        $con->execute([
            "success_id" => $_GET["uid"]
        ]);

        $one = $con->fetch(\PDO::FETCH_ASSOC);

        if (!$one) {
            redirectBack([
                "error" => "User Not found!",
                "code" => -1
            ]);
        }

        view('admin/success/edit', $one);
    }

    public function delete($id)
    {
        if (is_numeric($id)) {
            $select = (db::connect())->prepare("SELECT success_id,success_image FROM sm_success WHERE success_id=:success_id");

            $select->execute(array(
                'success_id' => $id
            ));

            $up2 = $select->fetch(\PDO::FETCH_ASSOC);
            if (!$up2) {
                redirectBack([
                    "error" => "User not found!",
                    "code" => -1
                ]);
            }
            if (file_exists(assetImage('/asset/uploads/Success/' . $up2['success_image']))) {
                unlink(assetImage('/asset/uploads/Success/' . $up2['success_image']));
            }

            $delete = (db::connect())->prepare("DELETE FROM `sm_success` WHERE success_id=:success_id");

            $up = $delete->execute(array(
                'success_id' => $id
            ));

            if (!$up) {
                redirectBack([
                    "error" => "Something went wrong!",
                    "code" => -1
                ]);
            } else {
                redirectBack([
                    "code" => 2
                ]);
            }
        } else {
            redirectBack([
                "error" => "ID Wrong type",
                "code" => -1
            ]);
        }
    }

    public function store()
    {
        $result2 = Validations::validate($_FILES, [
            'image' => ['required' => true, 'mime' => ['jpg', 'jpeg', 'png']],
        ]);
        if ($result2['code'] != true) {
            redirectBack([
                "error" => $result2['message'],
                "code" => -1
            ]);
        }

        $result = Validations::validate($_POST, [
            'name' => ['required' => true],
            'uni' => ['required' => true],
            'faculty' => ['required' => true],
            'degree' => ['required' => true],
        ]);

        if ($result['code'] == true) {

            $image = null;

            $path = assetImage('asset/uploads/Success/');
            $file = $_FILES['image'];
            $file_ext = explode('.', $file['name']);
            $file_ext = strtolower(end($file_ext));
            $file_full_name = uniqid() . '.' . $file_ext;
            $file_saved = $path . $file_full_name;

            $mm = move_uploaded_file($file['tmp_name'], $file_saved);
            if ($file_full_name) {
                $con = (db::connect())->prepare("INSERT INTO sm_success SET
                    success_image=:success_image,
                    success_fullname=:success_fullname,
                    success_uni=:success_uni,
                    success_faculty=:success_faculty,
                    success_degree=:success_degree
                    ");

                $up = $con->execute([
                    "success_image" => $file_full_name,
                    "success_fullname" => $_POST['name'],
                    "success_uni" => $_POST['uni'],
                    'success_faculty' => $_POST['faculty'],
                    'success_degree' => $_POST['degree']
                ]);

                if (!$up) {
                    redirectBack([
                        "error" => "Something went wrong! 3",
                        "code" => -1
                    ]);
                } else {
                    $_SESSION["back"]['code'] = 1;
                    $ur = site_url_admin("success");
                    header("Location:" . $ur);
                }
            } else {
                redirectBack([
                    "error" => "Something went wrong! 2",
                    "code" => -1
                ]);
            }
        } else {
            redirectBack([
                "error" => $result['message'],
                "code" => -1
            ]);
        }
    }

    public function update()
    {
        $result3 = Validations::validate($_GET, [
            'uid' => ['required' => true, 'type' => 'integer']
        ]);
        if ($result3['code'] != true) {
            redirectBack([
                "error" => "Not found!",
                "code" => -1
            ]);
        }

        $result = Validations::validate($_POST, [
            'uni' => ['required' => true],
            'faculty' => ['required' => true],
            'degree' => ['required' => true],
            'name' => ['required' => true],
        ]);

        if ($result['code'] != true) {
            redirectBack([
                "error" => $result['message'],
                "code" => -1
            ]);
        }

        $query = "SELECT * FROM sm_success where success_id=:success_id";
        $con = (db::connect())->prepare($query);
        $con->execute([
            "success_id" => $_GET['uid']
        ]);

        $user = $con->fetch(\PDO::FETCH_ASSOC);

        if (!$user) {
            redirectBack([
                "error" => "User Tapilmadi",
                "code" => -1
            ]);
        }
        $image = $user['success_image'];
        
        if ($_FILES['image']['name']) {
            $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
            if (!in_array($ext, ['jpg', 'jpeg', 'png'])) {
                redirectBack([
                    "error" => "Only jpg,Png,Jpeg extentions files accepting",
                    "code" => -1
                ]);
            } else {
                $path = assetImage('asset/uploads/Success/');
                if (file_exists($path . $image)) {
                    unlink($path . $image);
                }
                $image = null;
                $file = $_FILES['image'];
                $file_ext = explode('.', $file['name']);
                $file_ext = strtolower(end($file_ext));
                $file_full_name = uniqid() . '.' . $file_ext;
                $image = $file_full_name;

                $file_saved = $path . $file_full_name;

                $mm = move_uploaded_file($file['tmp_name'], $file_saved);
     
            }
        }


        $con = (db::connect())->prepare("UPDATE sm_success SET
                    success_image=:success_image,
                    success_fullname=:success_fullname,
                    success_uni=:success_uni,
                    success_faculty=:success_faculty,
                    success_degree=:success_degree
                    ");

                $up = $con->execute([
                    "success_image" => $file_full_name,
                    "success_fullname" => $_POST['name'],
                    "success_uni" => $_POST['uni'],
                    'success_faculty' => $_POST['faculty'],
                    'success_degree' => $_POST['degree']
                ]);
    
        if (!$up) {
        
            redirectBack([
                "error" => "Something went wrong!",
                "code" => -1
            ]);
        } else {
            $_SESSION["back"]['code'] = 1;
            $ur = site_url_admin("success");
            header("Location: " . $ur);
        }
    }
}
