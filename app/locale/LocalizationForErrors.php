<?php

namespace app\locale;

class LocalizationForErrors
{
    public static $local = [
        'az' => [
            'not_isset' => "This %s field not exits",
            'required' => "This %s field required",
            'min' => "This %s field min %d",
            'max' => "This %s field max %d",
            'email' => "This %s field must be email",
            'image_ext' => "This %s field must be correct type image",
            'type' => "This %s field must be correct type",
            'confirm_p' => "This %s field must be the same value Password",
        ],
        'en' => [
            'not_isset' => "This %s field not exits",
            'required' => "This %s field required",
            'min' => "This %s field min %d",
            'max' => "This %s field max %d",
            'email' => "This %s field must be email",
            'image_ext' => "This %s field must be correct type image",
            'type' => "This %s field must be correct type",
            'confirm_p' => "This %s field must be the same value Password",
        ],
        'ru' => [
            'not_isset' => "This %s field not exits",
            'required' => "This %s field required",
            'min' => "This %s field min %d",
            'max' => "This %s field max %d",
            'email' => "This %s field must be email",
            'image_ext' => "This %s field must be correct type image",
            'type' => "This %s field must be correct type",
            'confirm_p' => "This %s field must be the same value Password",
        ]
    ];

    public static function getLocal($lang, $lang_key)
    {
        return self::$local[$lang][$lang_key];
    }
}