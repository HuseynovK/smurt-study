<?php
require_once assetFileAdmin("layouts/header.php");
global $perpage;
?>

<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="content-header">Blog</div>
                </div>
            </div>

            <?php
            if (isset($_SESSION['back']['code'])):
                if ($_SESSION['back']['code'] == -1): ?>
                    <div class="alert alert-danger">
                        <?= $_SESSION['back']['error'] ?>
                    </div>
                    <?php unset($_SESSION['back']) ?>
                <?php endif; ?>
            <?php endif; ?>
            <?php
            if (isset($_SESSION['back']['code'])):
                if ($_SESSION['back']['code'] == 2): ?>
                    <div class="alert alert-success">
                        Deleted Succesfully
                    </div>
                    <?php unset($_SESSION['back']) ?>
                <?php endif; ?>
            <?php endif; ?>

            <?php
            if (isset($_SESSION['back']['code'])):
                if ($_SESSION['back']['code'] == 1): ?>
                    <div class="alert alert-success">
                        Succesfully Inserted
                    </div>
                    <?php unset($_SESSION['back']) ?>
                <?php endif; ?>
            <?php endif; ?>
            <!-- DOM - jQuery events table -->
            <section id="dom">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a href="<?= site_url_admin('add_blog') ?>" class="btn btn-primary">Add Blog</a>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered dom-jQuery-events">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Image</th>
                                                <th>Author Image</th>
                                                <th>Title</th>
                                                <th>Author</th>
                                                <th>Context</th>
                                                <th>Time</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($data['blog'] as $value): ?>
                                                <tr>
                                                    <td><?= $value['blog_id'] ?></td>
                                                    <td><img style="width: 200px;height: 200px;"
                                                             src="<?= assetUrl('/uploads/Blog/' . $value['blog_image']) ?>"
                                                             alt=""></td>
                                                    <td><img style="width: 200px;height: 200px;"
                                                             src="<?= assetUrl('/uploads/Blog/' . $value['blog_author_image']) ?>"
                                                             alt=""></td>
                                                    <td><?= $value['blog_title'] ?></td>
                                                    <td><?= $value['blog_author'] ?></td>
                                                    <td><?= $value['blog_context'] ?></td>
                                                    <td><?= $value['blog_time'] ?></td>
                                                    <td>
                                                        <form action="<?= site_url_admin("delete_blog/" . $value['blog_id']) ?>"
                                                              method="post" style="margin-bottom: 7px">
                                                            <input type="hidden" name="_token"
                                                                   value="<?= csrf_token() ?>">
                                                            <button class="btn-sm btn-danger" type="submit"> Delete
                                                            </button>
                                                        </form>
                                                        <a href="<?= site_url_admin("edit_blog/?uid=" . $value['blog_id']) ?>"
                                                           class="btn-sm btn-success" type="submit"> Update
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Image</th>
                                                <th>Author Image</th>
                                                <th>Title</th>
                                                <th>Author</th>
                                                <th>Context</th>
                                                <th>Time</th>
                                                <th>Actions</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        <div class="float-right mr-4">
                                            <?php paginate(count($data['blog']), $perpage); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- DOM - jQuery events table -->
        </div>
    </div>
</div>
<?php
require_once assetFileAdmin("layouts/footer.php");
?>
