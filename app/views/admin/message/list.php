<?php
require_once assetFileAdmin("layouts/header.php");
global $perpage;
?>

<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="content-header">Messages</div>
                </div>
            </div>

            <?php
            if (isset($_SESSION['back']['code'])) :
                if ($_SESSION['back']['code'] == -1) : ?>
                    <div class="alert alert-danger">
                        <?= $_SESSION['back']['error'] ?>
                    </div>
                    <?php unset($_SESSION['back']) ?>
                <?php endif; ?>
            <?php endif; ?>
            <?php
            if (isset($_SESSION['back']['code'])) :
                if ($_SESSION['back']['code'] == 2) : ?>
                    <div class="alert alert-success">
                        Deleted Succesfully
                    </div>
                    <?php unset($_SESSION['back']) ?>
                <?php endif; ?>
            <?php endif; ?>

            <?php
            if (isset($_SESSION['back']['code'])) :
                if ($_SESSION['back']['code'] == 1) : ?>
                    <div class="alert alert-success">
                        Succesfully Inserted
                    </div>
                    <?php unset($_SESSION['back']) ?>
                <?php endif; ?>
            <?php endif; ?>
            <!-- DOM - jQuery events table -->
            <section id="dom">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered dom-jQuery-events">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Surname</th>
                                                    <th>Phone</th>
                                                    <th>Context</th>
                                                    <th>Date</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($data['messages'] as $value) : ?>
                                                    <tr>
                                                        <td><?= $value['message_id'] ?></td>
                                                        <td><?= $value['message_name'] ?></td>
                                                        <td><?= $value['message_surname'] ?></td>
                                                        <td><?= $value['message_phone'] ?></td>
                                                        <td><?= $value['message_context'] ?></td>
                                                        <td><?= $value['message_time'] ?></td>
                                                        <td>
                                                            <form action="<?= site_url_admin("delete_message/" . $value['message_id']) ?>" method="post" style="margin-bottom: 7px">
                                                                <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                                                                <button class="btn-sm btn-danger" type="submit"> Delete
                                                                </button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Surname</th>
                                                    <th>Phone</th>
                                                    <th>Context</th>
                                                    <th>Date</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <div class="float-right mr-4">
                                            <?php paginate(count($data['messages']), $perpage); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- DOM - jQuery events table -->
        </div>
    </div>
</div>
<?php
require_once assetFileAdmin("layouts/footer.php");
?>