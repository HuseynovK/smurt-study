<!-- START Notification Sidebar-->
<!-- END Notification Sidebar-->
</div>
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN VENDOR JS-->
<script src="<?= assetUrl('app-assets/vendors/js/vendors.min.js') ?>"></script>
<script src="<?= assetUrl('app-assets/vendors/js/switchery.min.js') ?>"></script>
<!--<script src="--><? //= assetUrl('app-assets/vendors/js/chartist.min.js') ?><!--"></script>-->
<script src="<?= assetUrl('app-assets/js/core/app-menu.js') ?>"></script>
<script src="<?= assetUrl('app-assets/js/core/app.js') ?>"></script>
<script src="<?= assetUrl('app-assets/js/notification-sidebar.js') ?>"></script>
<script src="<?= assetUrl('app-assets/js/customizer.js') ?>"></script>
<script src="<?= assetUrl('app-assets/js/scroll-top.js') ?>"></script>
<!--<script src="--><? //= assetUrl('app-assets/js/dashboard1.js') ?><!--"></script>-->
<script src="<?= assetUrl('assets/js/scripts.js') ?>"></script>

<script src="<?= assetUrl('app-assets/vendors/js/datatable/jquery.dataTables.min.js') ?>"></script>
<script src="<?= assetUrl('app-assets/vendors/js/datatable/dataTables.bootstrap4.min.js') ?>"></script>
<script src="<?= assetUrl('app-assets/vendors/js/datatable/dataTables.buttons.min.js') ?>"></script>
<script src="<?= assetUrl('app-assets/vendors/js/datatable/buttons.html5.min.js') ?>"></script>
<script src="<?= assetUrl('app-assets/vendors/js/datatable/buttons.print.min.js') ?>"></script>
<script src="<?= assetUrl('app-assets/vendors/js/datatable/jszip.min.js') ?>"></script>
<script src="<?= assetUrl('app-assets/vendors/js/datatable/vfs_fonts.js') ?>"></script>
<script src="<?= assetUrl('app-assets/vendors/js/datatable/pdfmake.min.js') ?>"></script>
<script src="<?= assetUrl('app-assets/vendors/js/datatable/dt-advanced-initialization.js') ?>"></script>

<script src="<?= assetUrl('app-assets/js/form-inputs.js') ?>"></script>
</body>
<!-- END : Body-->

</html>