<?php

$menus = [
    'users' => 'Users',
    'education' => 'Abroad Education',
    'course' => 'Language Course',
    'service' => 'Services',
    'success' => 'Successes',
    'country' => 'Country',
    'blog' => 'Blog',
    'messages' => 'Messages'
]

?>
<!DOCTYPE html>
<html class="loading" lang="en">
<!-- BEGIN : Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description"
          content="Apex admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords"
          content="admin template, Apex admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Smart Study</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?= assetUrl('resources/smartfav.ico') ?>">
    <link rel="shortcut icon" type="image/x-icon" href="<?= assetUrl('resources/smartfav.ico') ?>">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900%7CMontserrat:300,400,500,600,700,800,900"
          rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <!-- font icons-->
    <!--    <link rel="stylesheet" type="text/css" href="/cms/app/asset/app-assets/fonts/feather/style.min.css ">-->
    <link rel="stylesheet" type="text/css" href="<?= assetUrl('app-assets/fonts/feather/style.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= assetUrl('app-assets/fonts/simple-line-icons/style.css') ?>">
    <link rel="stylesheet" type="text/css"
          href="<?= assetUrl('app-assets/fonts/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" type="text/css"
          href="<?= assetUrl('app-assets/vendors/css/perfect-scrollbar.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= assetUrl('app-assets/vendors/css/prism.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= assetUrl('app-assets/vendors/css/switchery.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= assetUrl('app-assets/vendors/css/chartist.min.css') ?>">
    <!-- END VENDOR CSS-->
    <!-- BEGIN APEX CSS-->
    <link rel="stylesheet" type="text/css" href="<?= assetUrl('app-assets/css/bootstrap.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= assetUrl('app-assets/css/bootstrap-extended.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= assetUrl('app-assets/css/colors.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= assetUrl('app-assets/css/components.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= assetUrl('app-assets/css/themes/layout-dark.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= assetUrl('app-assets/css/plugins/switchery.css') ?>">
    <!-- END APEX CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?= assetUrl('app-assets/css/pages/dashboard1.css') ?>">
    <!-- END Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?= assetUrl('assets/css/style.css') ?>">
    <!-- END: Custom CSS-->
    <link rel="stylesheet" type="text/css"
          href="<?= assetUrl('app-asset/vendors/css/datatables/dataTables.bootstrap4.min.css') ?>">
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

    <!-- END VENDOR CSS-->
</head>
<!-- END : Head-->

<!-- BEGIN : Body-->

<body class="vertical-layout vertical-menu 2-columns  navbar-sticky" data-menu="vertical-menu" data-col="2-columns">

<nav class="navbar navbar-expand-lg navbar-light header-navbar navbar-fixed">
    <div class="container-fluid navbar-wrapper">
        <div class="navbar-header d-flex">
            <div class="navbar-toggle menu-toggle d-xl-none d-block float-left align-items-center justify-content-center"
                 data-toggle="collapse"><i class="ft-menu font-medium-3"></i></div>
            <ul class="navbar-nav">
                <li class="nav-item mr-2 d-none d-lg-block"><a class="nav-link apptogglefullscreen"
                                                               id="navbar-fullscreen" href="javascript:;"><i
                                class="ft-maximize font-medium-3"></i></a></li>
                <li class="nav-item nav-search"><a class="nav-link nav-link-search" href="javascript:"><i
                                class="ft-search font-medium-3"></i></a>
                    <div class="search-input">
                        <div class="search-input-icon"><i class="ft-search font-medium-3"></i></div>
                        <input class="input" type="text" placeholder="Explore Apex..." tabindex="0"
                               data-search="template-search">
                        <div class="search-input-close"><i class="ft-x font-medium-3"></i></div>
                        <ul class="search-list"></ul>
                    </div>
                </li>
            </ul>
        </div>
        <div class="navbar-container">
            <div class="collapse navbar-collapse d-block" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="i18n-dropdown dropdown nav-item mr-2">
                        <form action="<?= site_url_admin('logout') ?>" method="get">
                            <button type="submit" class="btn btn-danger">Logout</button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- Navbar (Header) Ends-->

<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="wrapper">


    <!-- main menu-->
    <!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
    <div class="app-sidebar menu-fixed" data-background-color="man-of-steel"
         data-image="<?= assetUrl('app-assets/img/sidebar-bg/01.jpg') ?>"
         data-scroll-to-active="true">
        <!-- main menu header-->
        <!-- Sidebar Header starts-->
        <div class="sidebar-header">
            <div class="logo clearfix"><a class="logo-text float-left" href="<?=site_url_admin()?>">
                    <div class="logo-img"><img width="auto" height="50"
                                src="<?= assetUrl('resources/smartfav.ico') ?>"
                                alt="Apex Logo"/></div>
                    <span class="text" style="font-size: 15px;">Smart Study</span>
                </a><a class="nav-toggle d-none d-lg-none d-xl-block" id="sidebarToggle" href="javascript:;"><i
                            class="toggle-icon ft-toggle-right" data-toggle="expanded"></i></a><a
                        class="nav-close d-block d-lg-block d-xl-none" id="sidebarClose" href="javascript:;"><i
                            class="ft-x"></i></a></div>
        </div>
        <!-- Sidebar Header Ends-->
        <!-- / main menu header-->
        <!-- main menu content-->
        <div class="sidebar-content main-menu-content">
            <div class="nav-container">
                <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                    <?php foreach($menus as $url => $menu): ?>
                        <li class="nav-item">
                            <a href="<?= site_url_admin($url) ?>">
                                <i class="ft-book-open"></i>
                                <span
                                    class="menu-title"
                                    data-i18n="Dashboard"><?=$menu?>
                                </span>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <!-- main menu content-->
        <div class="sidebar-background"></div>
        <!-- main menu footer-->
        <!-- include includes/menu-footer-->
        <!-- main menu footer-->
        <!-- / main menu-->
    </div>
