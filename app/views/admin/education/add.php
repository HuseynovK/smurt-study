<?php
require_once assetFileAdmin("layouts/header.php");
//var_dump($_SESSION['back']);
//die();
?>

<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <?php
            if (isset($_SESSION['back']['error'])): ?>
                <div class="alert alert-danger">
                    <?= $_SESSION['back']['error'] ?>
                </div>
            <?php endif; ?>
            <?php unset($_SESSION['back']) ?>
            <div class="row">
                <div class="col-12">
                    <div class="content-header">Education Add</div>
                </div>
            </div>
            <!-- Basic Inputs start -->
            <section id="basic-input">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <form method="post" enctype="multipart/form-data">
                                                <input type="hidden" value="<?= generate_csrf_and_token() ?>"
                                                       name="_token">
                                                <fieldset class="form-group">
                                                    <input type="file" class="form-control" id="basicInput"
                                                           name="image" required>
                                                </fieldset>
                                                <fieldset class="form-group">
                                                    <input type="text" class="form-control" id="basicInput"
                                                           name="name" required
                                                           placeholder="Enter Name">
                                                </fieldset>
                                                <button class="btn btn-success float-right" type="submit">Add
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Basic Inputs end -->
        </div>
    </div>
</div>

<?php
require_once assetFileAdmin("layouts/footer.php");
?>
