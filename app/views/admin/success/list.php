<?php
require_once assetFileAdmin("layouts/header.php");
global $perpage;
?>

<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="content-header">Success</div>
                </div>
            </div>

            <?php
            if (isset($_SESSION['back']['code'])) :
                if ($_SESSION['back']['code'] == -1) : ?>
                    <div class="alert alert-danger">
                        <?= $_SESSION['back']['error'] ?>
                    </div>
                    <?php unset($_SESSION['back']) ?>
                <?php endif; ?>
            <?php endif; ?>
            <?php
            if (isset($_SESSION['back']['code'])) :
                if ($_SESSION['back']['code'] == 2) : ?>
                    <div class="alert alert-success">
                        Deleted Succesfully
                    </div>
                    <?php unset($_SESSION['back']) ?>
                <?php endif; ?>
            <?php endif; ?>

            <?php
            if (isset($_SESSION['back']['code'])) :
                if ($_SESSION['back']['code'] == 1) : ?>
                    <div class="alert alert-success">
                        Succesfully Inserted
                    </div>
                    <?php unset($_SESSION['back']) ?>
                <?php endif; ?>
            <?php endif; ?>
            <!-- DOM - jQuery events table -->
            <section id="dom">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a href="<?= site_url_admin('add_success') ?>" class="btn btn-primary">Add Success</a>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered dom-jQuery-events">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Image</th>
                                                    <th>FullName</th>
                                                    <th>Univercity</th>
                                                    <th>Faculty</th>
                                                    <th>Degree</th>
                                                    <th>Date</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($data['success'] as $value) : ?>
                                                    <tr>
                                                        <td><?= $value['success_id'] ?></td>
                                                        <td><img style="width: 200px;height: auto;" src="<?= assetUrl('uploads/Success/' . $value['success_image']) ?>" alt="Success"></td>
                                                        <td><?=$value['success_fullname']?></td>
                                                        <td><?= $value['success_uni'] ?></td>
                                                        <td><?= $value['success_faculty'] ?></td>
                                                        <td><?= $value['success_degree'] ?></td>
                                                        <td><?= $value['success_time'] ?></td>
                                                        <td>
                                                            <form action="<?= site_url_admin("delete_success/" . $value['success_id']) ?>" method="post" style="margin-bottom: 7px">
                                                                <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                                                                <button class="btn-sm btn-danger" type="submit"> Delete
                                                                </button>
                                                            </form>
                                                            <a href="<?= site_url_admin("edit_success/?uid=" . $value['success_id']) ?>" class="btn-sm btn-success" type="submit"> Update
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Image</th>
                                                    <th>FullName</th>
                                                    <th>Univercity</th>
                                                    <th>Faculty</th>
                                                    <th>Degree</th>
                                                    <th>Date</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <div class="float-right mr-4">
                                            <?php paginate(count($data['success']), $perpage); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- DOM - jQuery events table -->
        </div>
    </div>
</div>
<?php
require_once assetFileAdmin("layouts/footer.php");
?>