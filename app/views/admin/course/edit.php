<?php
require_once assetFileAdmin("layouts/header.php");
?>

<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <?php
            if (isset($_SESSION['back']['error'])): ?>
                <div class="alert alert-danger">
                    <?= $_SESSION['back']['error'] ?>
                </div>
            <?php endif; ?>
            <?php unset($_SESSION['back']) ?>
            <div class="row">
                <div class="col-12">
                    <div class="content-header">Course Edit</div>
                </div>
            </div>
            <!-- Basic Inputs start -->
            <section id="basic-input">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <form method="post" enctype="multipart/form-data">
                                                <input type="hidden" value="<?= generate_csrf_and_token() ?>"
                                                       name="_token">
                                                <fieldset class="form-group">
                                                    <label>Image</label>
                                                    <input type="file" class="form-control" id="basicInput"
                                                           name="image">
                                                </fieldset>
                                                <fieldset class="form-group">
                                                    <input type="text" class="form-control" id="basicInput"
                                                           name="name" required
                                                           value="<?= $data['course_name'] ?>"
                                                           placeholder="Enter Name">
                                                </fieldset>
                                                <fieldset class="form-group">
                                                    <label for="">Conetext</label>
                                                    <textarea name="context" id="" cols="30" required
                                                              rows="10"><?= $data['course_context'] ?></textarea>
                                                </fieldset>
                                                <button class="btn btn-success float-right" type="submit">Submit
                                                </button>
                                                <img style="width: 200px;height: 200px;"
                                                     src="<?= assetUrl('uploads/Course/' . $data['course_image']) ?>"
                                                     alt="">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Basic Inputs end -->
        </div>
    </div>
</div>
<script>
    CKEDITOR.editorConfig = function (config) {
        config.language = 'en';
        config.autoParagraph = false;
    };
    CKEDITOR.replace('context');
</script>
<?php
require_once assetFileAdmin("layouts/footer.php");
?>
