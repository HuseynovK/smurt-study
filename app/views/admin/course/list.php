<?php
require_once assetFileAdmin("layouts/header.php");
global $perpage;
?>

<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="content-header">Language Course</div>
                </div>
            </div>

            <?php
            if (isset($_SESSION['back']['code'])) :
                if ($_SESSION['back']['code'] == -1) : ?>
                    <div class="alert alert-danger">
                        <?= $_SESSION['back']['error'] ?>
                    </div>
                    <?php unset($_SESSION['back']) ?>
                <?php endif; ?>
            <?php endif; ?>
            <?php
            if (isset($_SESSION['back']['code'])) :
                if ($_SESSION['back']['code'] == 2) : ?>
                    <div class="alert alert-success">
                        Deleted Succesfully
                    </div>
                    <?php unset($_SESSION['back']) ?>
                <?php endif; ?>
            <?php endif; ?>

            <?php
            if (isset($_SESSION['back']['code'])) :
                if ($_SESSION['back']['code'] == 1) : ?>
                    <div class="alert alert-success">
                        Succesfully Inserted
                    </div>
                    <?php unset($_SESSION['back']) ?>
                <?php endif; ?>
            <?php endif; ?>
            <!-- DOM - jQuery events table -->
            <section id="dom">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a href="<?= site_url_admin('add_course') ?>" class="btn btn-primary">Add Course</a>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered dom-jQuery-events">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Context</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($data['course'] as $value) : ?>
                                                    <tr>
                                                        <td><?= $value['course_id'] ?></td>
                                                        <td><img style="width: 200px;height: auto;" src="<?= assetUrl('uploads/Course/' . $value['course_image']) ?>" alt="Course"></td>
                                                        <td><?= $value['course_name'] ?></td>
                                                        <td><?= $value['course_context'] ?></td>
                                                        <td>
                                                            <form action="<?= site_url_admin("delete_course/" . $value['course_id']) ?>" method="post" style="margin-bottom: 7px">
                                                                <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                                                                <button class="btn-sm btn-danger" type="submit"> Delete
                                                                </button>
                                                            </form>
                                                            <a href="<?= site_url_admin("edit_course/?uid=" . $value['course_id']) ?>" class="btn-sm btn-success" type="submit"> Update
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Context</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <div class="float-right mr-4">
                                            <?php paginate(count($data['course']), $perpage); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- DOM - jQuery events table -->
        </div>
    </div>
</div>
<?php
require_once assetFileAdmin("layouts/footer.php");
?>