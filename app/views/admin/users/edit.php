<?php
require_once assetFileAdmin("layouts/header.php");
?>

<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <?php
            if (isset($_SESSION['back']['error'])): ?>
                <div class="alert alert-danger">
                    <?= $_SESSION['back']['error'] ?>
                </div>
            <?php endif; ?>
            <?php unset($_SESSION['back']) ?>
            <!--            <div class="row">-->
            <!--                <div class="col-12">-->
            <!--                    <div class="content-header">Add User</div>-->
            <!--                </div>-->
            <!--            </div>-->
            <!-- Basic Inputs start -->
            <section id="basic-input">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <form method="post">
                                                <input type="hidden" value="<?= generate_csrf_and_token() ?>"
                                                       name="_token">
                                                <fieldset class="form-group">
                                                    <input type="text" class="form-control" id="basicInput"
                                                           name="fullname"
                                                           value="<?= $data['fullname'] ?>"
                                                           placeholder="Enter FullName">
                                                </fieldset>
                                                <fieldset class="form-group">
                                                    <input type="email" class="form-control" id="basicInput"
                                                           name="email"
                                                           value="<?= $data['email'] ?>"
                                                           placeholder="Enter Email">
                                                </fieldset>
                                                <fieldset class="form-group">
                                                    <input type="password" class="form-control" id="basicInput"
                                                           name="password"
                                                           placeholder="Enter Password">
                                                </fieldset>
                                                <fieldset class="form-group">
                                                    <input type="password" class="form-control" id="basicInput"
                                                           name="confirm_password"
                                                           placeholder="Repeat Password">
                                                </fieldset>
                                                <button class="btn btn-success float-right" type="submit">Submit
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Basic Inputs end -->
        </div>
    </div>
</div>

<?php
require_once assetFileAdmin("layouts/footer.php");
?>
