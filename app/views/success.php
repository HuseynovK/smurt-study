<?php require_once assetFile('layouts/header.php')?>
<div class="main-container">
        <div class="main">
            <a href="<?=site_url('')?>"><div id="arrow2" ><img src="<?=assetUrl('resources/images/kecid.ag.svg')?>" alt=""></div></a>            <h1 id=line>Uğurlarımız</h1>
            <div class="country-cards">
                <?php foreach($data['success'] as $success): ?>
                    <div class="success2">
                        <div id="profile"><img  src="<?=assetUrl('uploads/Success/'.$success['success_image'])?>" alt="<?=$success['success_fullname']?>"></div>
                        <div class="box">
                            <img src="<?=assetUrl('resources/images/istifadecci.svg')?>" alt="User">
                            <p><?=$success['success_fullname']?></p>
                        </div>
                            <hr/>
                        <div class="box">
                            <img src="<?=assetUrl('resources/images/university(1).svg')?>" alt="Univercity">
                            <p><?=$success['success_uni']?></p>
                        </div>
                            <hr/>
                        <div class="box">
                            <img src="<?=assetUrl('resources/images/ixtisas.svg')?>" alt="Faculty">
                            <p><?=$success['success_degree']?></p>
                        </div>
                            <hr/>
                        <div class="box">
                            <img src="<?=assetUrl('resources/images/score.svg')?>" alt="Score">
                            <p><?=$success['success_degree']?></p>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
<?php require_once assetFile('layouts/footer.php')?>
