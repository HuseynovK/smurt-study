<?php require_once assetFile('layouts/header.php')?>
<div class="main-container">
        <div class="main">
            <a href="<?=site_url('')?>"><div id="arrow2" ><img src="<?=assetUrl('resources/images/kecid.ag.svg')?>" alt="Kecid"></div></a>            <h1 id=line>Ödənişsiz Konsultasiya</h1>
            <div class="aside">
                <div id="singleimg1">
                    <img src="<?=assetUrl('uploads/Service/'.$data['service']['service_single_image'])?>" alt="<?=$data['service']['service_name']?>">
                </div>
                <div id="singleinfo">
                    <span>
                        <?=$data['service']['service_context']?>
                    </span>
                </div>
            </div>
        </div>
    </div>
<?php require_once assetFile('layouts/footer.php')?>