<?php require_once assetFile('layouts/header.php')?>
<div class="main-container">
        <div class="main">
            <a href="<?=site_url('')?>"><div id="arrow2" ><img src="<?=assetUrl('resources/images/kecid.ag.svg')?>" alt="Kecid"></div></a>            
            <h1 id=line>Ölkələr</h1>
                <div class="country-cards">
                    <?php foreach($data['country'] as $country): ?>
                        <a href="<?=site_url('country_detail/'.$country['country_id'])?>">
                            <div style="background: url(<?=assetUrl('uploads/Country/'.$country['country_photo_path'])?>) center/cover;" class="icons1" >
                                <div class="country-text">
                                    <span><?=$country['country_name']?></span>
                                    <span><?=substr($country['country_context'],0,100)?></span>
                                </div>
                            </div>
                        </a>
                    <?php endforeach ?>
                </div>
        </div>
    </div>
<?php require_once assetFile('layouts/footer.php')?>
