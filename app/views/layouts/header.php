<?php


$menus = [
    'studyabroad' => 'Xaricdə təhsil',
    'country' => 'Ölkələr',
    'service' => 'Xidmətlərimiz',
    'blog' => 'Bloq',
    'success' => 'Uğurlarımız',
    'course' => 'Dil kursları',
    'contact' => 'Əlaqə'
];


?>
<!DOCTYPE html>
<html lang="az">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"crossorigin="anonymous"></script>
    <link rel="stylesheet" href="<?=assetUrl('resources/css/style.css')?>" />
    <link rel="stylesheet" href="<?=assetUrl('resources/css/responsive.css')?>" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css"integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous" />
    <link rel="icon" type="image/png" sizes="32x32" href="<?=assetUrl('/resources/images/smartfav.ico')?>">
    <title>Smart Study</title>
</head>
<body>
    <nav class=" navbar navbar-expand-lg navbar-light bg-light">
        <div class=" container-fluid d-flex">
            <a class="navbar-brand" href="<?=site_url('')?>"><img src="<?=assetUrl('resources/images/logosmart.png')?>" alt="Smart Study"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <?php foreach($menus as $key => $menu): ?>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="<?=site_url('#'.$key)?>"><?=$menu?></a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </nav>