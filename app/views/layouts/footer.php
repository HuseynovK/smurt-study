<div id="contact" class="footer-container">
        <div class="footer">
            <div class="contact">
                <h3>Bizimlə Əlaqəyə Keçin!</h3>
            </div>
            <hr>
            <div class="footer-flex">
                <div class="footer1">
                    <div>
                        <h4>Məlumat</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. <br> Lorem, ipsum dolor sit amet
                            consectetur adipisicing elit. PerspicLorem, <br>ipsum dolor sit amet consecte tur
                            adipisicing elit. Perspicgsrddtydia</p>
                    </div>
                    <div>
                        <h4>Əlaqə Nömrəsi</h4>
                        <a id="tel" style="color:#fff" href="tel:+994123103598">+994 12 310 35 98</a><br>
                        <a id="tel" style="color:#fff" href="tel:+994774967090">+994 77 496 70 90</a><br>
                        <a id="tel" style="color:#fff" href="tel:+994703967090">+994 70 396 70 90</a>
                    </div>
                    <div>
                        <h4>Ünvan</h4>
                        <p>Caspian Business Center, 6-cı mərtəbə<br> Cəfər Cabbarlı 40, Bakı, Azərbaycan</p>
                    </div>
                    <div>
                        <h4>Bizi İzləyin</h4>
                        <a
                            href="https://wa.me/+994703967090"><img
                                src="<?=assetUrl('resources/images/(whatsapp).svg')?>" alt="Whatsapp"></a>
                        <a href="https://www.instagram.com/smartstudy.ltd/"><img
                                src="<?=assetUrl('resources/images/(instagram).svg')?>" alt="Instagram"></a>
                        <a href="https://www.facebook.com/smartstudy.llc"><img src="<?=assetUrl('resources/images/(facebook).svg')?>"
                                alt=""></a>
                        
                    </div>
                </div>
                <div class="footer2">
                    <div>
                        <h4>Müraciət Et</h4>
                    </div>
                    <div>
                        <form method="post" id="form" action="<?=site_url('')?>">
                            <input type="hidden" value="<?= generate_csrf_and_token2() ?>"
                                                       name="_token">
                            <input class="out" name="name" type="text" placeholder="Ad">
                            <input class="out" name="phone" type="number" placeholder="Əlaqə Nöm.">
                            <input class="out" name="surname" type="text" placeholder="Soyad">
                            <select name="service">
                                <option value="" disabled selected hidden>Seç</option>
                                <option value="Xaricdə təhsil">Xaricdə təhsil</option>
                                <option value="Dil kursları">Dil kursları</option>
                                <option value="Hazırlıq">Hazırlıq</option>
                              </select>
                            <textarea name="message" id="biginput" cols="30" rows="10"></textarea>
                            <button id="submit">Göndər</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <p>Copyright &copy; 2021 <a href="http://deirvlon.com/az"><b>Deirvlon Technologies.</b></a></p>
                <p> All rights are reserved. </p>
            </div>
        </div>
    </div>
    </div>
</body>

</html>