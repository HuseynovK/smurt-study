<?php require_once assetFile('layouts/header.php')?>
<div class="main-container">
        <div class="main">
            <a href="<?=site_url('')?>"><div id="arrow2" ><img src="<?=assetUrl('resources/images/kecid.ag.svg')?>" alt="Kecid"></div></a>            
            <h1 id=line><?=$data['course']['course_name']?></h1>
            <div class="aside">
                <div id="singleimg">
                    <img src="<?=assetUrl('uploads/Course/'.$data['course']['course_image'])?>" alt="<?=$data['course']['course_name']?>">
                </div>
                <div id="singleinfo">
                    <span>
                        <?=$data['course']['course_context']?>
                    </span>
                </div>
            </div>
        </div>
    </div>
<?php require_once assetFile('layouts/footer.php')?>