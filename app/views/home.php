<?php require_once assetFile('layouts/header.php')?>
<div class="hero">
        <div class="hero-inner">
            <div class="registration">
                <h1>Xoş Gəlmisiniz!</h1>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis laudantium consequuntur odit
                    aliquam excepturi, quibusdam porro dolore ipsam alias nemo.</p>
                <a href="<?=site_url('#contact')?>" id="registr">Qeydiyyat</a>
            </div>
            <div class="hero-img">
                <img src="<?=assetUrl('resources/images/home.png')?>" alt="Home">
            </div>
        </div>
    </div>
    <div id="studyabroad"  class="education-container" style="background: url(<?=assetUrl('resources/images/bg.jpg')?>) center/cover ;">
        <div class="education">
            <div class="text-blue">
                <h1>Xaricdə Təhsil</h1>
                <p>Xaricdə təhsil almaq istəyən tələbələr üçün xüsusi yaradılmış xidmət </p>
            </div>
            <div class="icons-container">
                <?php foreach($data['education'] as $education): ?>
                    <div class="icons">
                        <div class="iconsinner">
                            <img src="<?=assetUrl('uploads/Education/'.$education['education_image'])?>" alt="<?=$education['education_name']?>">
                            <p class="p3"><?=$education['education_name']?></p>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
    <div id="country" class="country-container" style="background: url(<?=assetUrl('resources/images/xaricde.tehsil.back-min.jpg')?>) center/cover ;">
        <div class="country">
            <div class="text-white">
                <h1>Ölkələr</h1>
                <p>Xaricdə təhsil almaq istəyən tələbələr üçün xüsusi yaradılmış xidmət
                </p>
            </div>
            <div class="icons1-container">
                <?php foreach($data['country'] as $country): ?>
                    <a href="<?=site_url('country_detail/' . $country['country_id'])?>">
                        <div style="background: url(<?=assetUrl('uploads/Country/'.$country['country_photo_path'])?>) center/cover;" class="icons1">
                            <div class="country-text">
                                <span><?=$country['country_name']?></span>
                                <span><?=substr($country['country_context'],0,100)?></span>
                            </div>
                        </div>
                    </a>
                <?php endforeach?>
                <div id="arrow0">
                    <a href="<?=site_url('country')?>"><img src="<?=assetUrl('resources/images/kecid.mavi.svg')?>" alt="Kecid"></a>
                </div>
            </div>
            <div class="bottomarrow">
                <a href="<?=site_url('country')?>"><img src="<?=assetUrl('resources/images/kecid.mavi.svg')?>" alt="Kecid"></a>
            </div>
        </div>
    </div>
    </div>
    <div id="service" class="education1-container" style="background: url(<?=assetUrl('resources/images/bg.jpg')?>) center/cover ;">
        <div class="education1">
            <div class="text-blue">
                <h1>Xidmətlərimiz</h1>
                <p>Xaricdə təhsil almaq istəyən tələbələr üçün xüsusi yaradılmış xidmət </p>
            </div>
            <div class="icons-container">
                <?php foreach($data['service'] as $service): ?>
                    <div class="icons">
                        <a href="<?=site_url('service_detail/'.$service['service_id'])?>">
                            <div class="iconsinner">
                                <img src="<?=assetUrl('uploads/Service/'.$service['service_image'])?>" alt="<?=$service['service_name']?>">
                                <p class="p3"><?=$service['service_name']?></p>
                            </div>
                        </a>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
    <div id="blog" class="blog-container" style="background: url(<?=assetUrl('resources/images/xaricde.tehsil.back-min.jpg')?>) center/cover;">
        <div class="blog">
            <div class="text-white">
                <h1>Blog</h1>
                <p>Xaricdə təhsil almaq istəyən tələbələr üçün xüsusi yaradılmış xidmət
                <p>
            </div>
            <div class="carousel">
                <?php foreach($data['blog'] as $blog): ?>
                    <a href="<?=site_url('blog_detail/'.$blog['blog_id'])?>" class="card1" >
                        <div >
                            <img class="photo1" src="<?=assetUrl('uploads/Blog/'.$blog['blog_image'])?>" alt="<?=$blog['blog_title']?>" />
                            <img class="photo2" src="<?=assetUrl('uploads/Blog/'.$blog['blog_author_image'])?>" alt="<?=$blog['blog_author_name']?>" />
                            <div id="info">
                                <h1 class="infoh1"><?=$blog['blog_author']?></h1>
                                <hr />
                                <h1 class="infoh2"><?=$blog['blog_title']?></h1>
                                <span><?=substr($blog['blog_context'],0,50)?></span>
                            </div>
                        </div>
                    </a>
                <?php endforeach ?>
                <div id="arrow">
                    <a href="<?=site_url('blog')?>"><img src="<?=assetUrl('resources/images/kecid.ag.svg')?>" alt="Kecid"></a>
                </div> 
            </div>
            <div class="bottomarrow">
            <a href="<?=site_url('blog')?>"><img src="<?=assetUrl('resources/images/kecid.ag.svg')?>" alt="Kecid"></a>
            </div> 
        </div>
    </div>
    <div id="success" class="success-container" style="background: url(<?=assetUrl('resources/images/bg.jpg')?>) center/cover ;">
        <div class="success">
            <div class="text-blue">
                <h1>Uğurlarımız</h1>
                <p>Xaricdə təhsil almaq istəyən tələbələr üçün xüsusi yaradılmış xidmət
                <p>
            </div>
            <div class="success-0">
                <?php foreach($data['success'] as $success): ?>
                <div class="success1">
                    <div id="profile"><img src="<?=assetUrl('uploads/Success/'.$success['success_image'])?>" alt="<?=$success['success_fullname']?>"></div>
                    <div class="box">
                        <img src="<?=assetUrl('resources/images/istifadecci.svg')?>" alt="Name">
                        <p><?=$success['success_fullname']?></p>
                    </div>
                    <hr />
                    <div class="box">
                        <img src="<?=assetUrl('resources/images/university(1).svg')?>" alt="Univercity">
                        <p><?=$success['success_uni']?></p>
                    </div>
                    <hr />
                    <div class="box">
                        <img src="<?=assetUrl('resources/images/ixtisas.svg')?>" alt="Faculty">
                        <p><?=$success['success_faculty']?></p>
                    </div>
                    <hr />
                    <div class="box">
                        <img src="<?=assetUrl('resources/images/score.svg')?>" alt="Degree">
                        <p><?=$success['success_degree']?></p>
                    </div>
                </div>
                <?php endforeach ?>
                <div id="arrow">
                    <a href="<?=site_url('success')?>"><img src="<?=assetUrl('resources/images/kecid.ag.svg')?>" alt="Kecid"></a>
                </div>
            </div>
            <div class="bottomarrow">
                <a href="<?=site_url('success')?>"><img src="<?=assetUrl('resources/images/kecid.ag.svg')?>" alt="Kecid"></a>
            </div>
        </div>
    </div>
    <div id="course" class="language-container" style="background: url(<?=assetUrl('resources/images/xaricde.tehsil.back-min.jpg')?>) center/cover ;">
        <div class="language">
            <div class="text-white">
                <h1>Dil Kursları</h1>
                <p>Xaricdə təhsil almaq istəyən tələbələr üçün xüsusi yaradılmış xidmət
                <p>
            </div>
            <div class="lang-flex">
                
                <?php foreach($data['course'] as $course): ?>
            
                    <a href="<?=site_url('course_detail/'.$course['course_id'])?>" class="language-photo" style="background: url(<?=assetUrl('uploads/Course/'.$course['course_image'])?>) center/cover;">
                        <div>
                            <div class="whitebox">
                                <p class="p2"><?=$course['course_name']?></p>
                            </div>
                        </div>
                    </a>
           
                <?php endforeach ?>
                
            </div>
        </div>
    </div>
    <?php require_once assetFile('layouts/footer.php')?>
