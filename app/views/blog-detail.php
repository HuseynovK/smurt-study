<?php require_once assetFile('layouts/header.php')?>
<div class="main-container">
        <div class="main">
            <a href="<?=site_url('')?>"><div id="arrow2" ><img src="<?=assetUrl('resources/images/kecid.ag.svg')?>" alt="Kecid"></div></a>
            <h1 id=line>Blog</h1>
            <div class="full">
                <div class=" left">
                    <img src="<?=assetUrl('uploads/Blog/'.$data['blog']['blog_image'])?>" alt="<?=$data['blog']['blog_title']?>">
                        <h1><?=$data['blog']['blog_title']?></h1>
                    <span>
                        <?=$data['blog']['blog_context']?>
                    </span>
                </div>
                <div class="right">
                    <?php foreach($data['blogs'] as $blog):?>
                        <a href="<?=site_url('blog_detail/'.$blog['blog_id'])?>" class="card4" style="display:block" >
                            <img class="photo1" src="<?=assetUrl('uploads/Blog/'.$blog['blog_image'])?>" alt="<?=$blog['blog_title']?>" />
                            <img class="photo2" src="<?=assetUrl('uploads/Blog/'.$blog['blog_author_image'])?>" alt="<?=$blog['blog_author']?>" />
                            <div id="info">
                                <h1 class="infoh1"><?=$blog['blog_author']?></h1>
                                <hr />
                                <h1 class="infoh2"><?=$blog['blog_title']?></h1>
                                <p></p>
                            </div>
                    </a>
                    <?php endforeach?>
                </div>
            </div>
        </div>
    </div>
<?php require_once assetFile('layouts/footer.php')?>
