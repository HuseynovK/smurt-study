<?php
function paginate($say, $perpage = 10)
{
    global $main_url;

    $NUMPERPAGE = $perpage;
    $this_page = $main_url;
    $data = range(1, 150);
    $num_results = $say;

    if (!isset($_GET['page']) || !$page = intval($_GET['page'])) {
        $page = 1;
    }

    $tmp = [];
    for ($p = 1, $i = 0; $i < $num_results; $p++, $i += $NUMPERPAGE) {
        if ($page == $p) {
            // current page shown as bold, no link
            $tmp[] = "<b>{$p}</b>";
        } else {
            $tmp[] = "<a href=\"{$this_page}?page={$p}\" style='margin-top: 2px'>{$p}</a>";
        }
    }

    for ($i = count($tmp) - 3; $i > 1; $i--) {
        if (abs($page - $i - 1) > 2) {
            unset($tmp[$i]);
        }
    }

    if (count($tmp) > 1) {
        echo "<p>";

        if ($page > 1) {
            echo "<a href=\"{$this_page}?page=" . ($page - 1) . "\">&laquo; Prev</a> | ";
        }

        $lastlink = 0;
        foreach ($tmp as $i => $link) {
            if ($i > $lastlink + 3) {
                echo " ... ";
            } elseif ($i) {
                echo " | ";
            }
            echo $link;
            $lastlink = $i;
        }

        if ($page <= $lastlink) {
            echo " | <a href=\"{$this_page}?page=" . ($page + 1) . "\">Next &raquo;</a>";
        }

        echo "</p>\n\n";
    }
}

function paginate2($say, $perpage = 10)
{
    global $main_url;

    $NUMPERPAGE = $perpage;
    $this_page = $main_url;
    $data = range(1, 150);
    $num_results = $say;

    if (!isset($_GET['page']) || !$page = intval($_GET['page'])) {
        $page = 1;
    }

    $tmp = [];
    for ($p = 1, $i = 0; $i < $num_results; $p++, $i += $NUMPERPAGE) {
        if ($page == $p) {
            // current page shown as bold, no link
            $tmp[] = '<li class="pagination-item is-active"><a class="pagination-link">' . $p . '</a> </li>';
        } else {
            $tmp[] = '<li class="pagination-item">' . "<a href=\"{$this_page}?page={$p}\" style='margin-top: 2px'" . ' class="pagination-link"' . ">{$p}</a>";
        }
    }

    for ($i = count($tmp) - 3; $i > 1; $i--) {
        if (abs($page - $i - 1) > 2) {
            unset($tmp[$i]);
        }
    }

    if (count($tmp) > 1) {
        echo "<p>";

        if ($page > 1) {
            echo '<li class="pagination-item--wide first">' . "<a " . 'class="prev"' . " href=\"{$this_page}?page=" . ($page - 1) . "\"><</a></li>";
        }

        $lastlink = 0;
        foreach ($tmp as $i => $link) {
            if ($i > $lastlink + 3) {
                echo " ... ";
            } elseif ($i) {
                echo " ";
            }
            echo $link;
            $lastlink = $i;
        }

        if ($page <= $lastlink) {
            echo '<li class="pagination-item--wide last">' . " <a" . ' class="next" ' . " href=\"{$this_page}?page=" . ($page + 1) . "\">></a></li>";
        }

        echo "</p>\n\n";
    }
}