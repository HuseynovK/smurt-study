<?php
$langFile = [
    'az' => [
        'apps_ot' => 'Digər xidmətlər',
        'apps_port' => 'Portfolio',
        'apps' => 'App',
        'apps_ab' => 'Məlumat',
        'apps_h2' => 'Ən yaxşı onlayn mühitlərin yaradılmasına sizə kömək edəcəyik!',
        'apps_muted' => 'İnkar edə bilməyəcəyiniz fikirləri  <br> inkişaf etdirən yaradıcı tətbiqləri bizimlə <br><b>KƏŞF ET!</b>',
        'proq_serv' => 'Xidmətlərimiz',
        'proq' => 'PROQRAM TƏMİNATI',
        'proq_prod' => 'Məhsullarımız',
        'proq_h2' => 'Bizim peşəkarlığımız sizin uğurunuz üçündür!',
        'proq_text_muted' => 'Xüsusi təkliflərimiz ilə möhtəşəm<br> nəticələr əldə etmək üçün, indi
                    <br><b>MÜRACİƏT ET!</b>',
        'cihaz_prod' => 'Məhsullarımız',
        'cihaz_h2' => 'Bizim peşəkarlığımız sizin uğurunuz üçündür!',
        'cihaz_text_muted' => 'Xüsusi təkliflərimiz ilə möhtəşəm<br> nəticələr əldə etmək üçün, indi
                    <br><b>MÜRACİƏT ET!</b>',
        'smm_h2' => 'Bizim peşəkarlığımız sizin uğurunuz üçündür!',
        'smm_text_muted' => 'Xüsusi təkliflərimiz ilə möhtəşəm<br> nəticələr əldə etmək üçün, indi
                    <br><b>MÜRACİƏT ET!</b>',
        'smm_korp' => 'Koorperativ üslub',
        'smm_korp_det' => '<b>Korporativ üslub nədir?</b><br>

                        Korporativ üslub hər hansı şirkətin vizual görünüşünü, yəni şirkətin rənglərinin və markasının
                        fərqləndirici loqosunu, onun məhsul və ya xidmətinin seçilmə və yaddaqalma strategiyasını,
                        ticari quruluşunun strukturunu ifadə edən vizual, yazılı, səsli məhsullardan ibarət
                        üslubdur.<br>

                        <b>Korporativ üslubun əhəmiyyəti:</b><br>

                        Bazarda bir şirkətin olması onun dəyərləri və ideologiyası ilə xarakterizə olunur. Digərlərindən
                        fərqlənmək üçün şirkətin fərqli üslublara ehtiyacı var. Vaxt keçdikcə təşkilatın uğuru şöhrət
                        qazanacaqdır. Beləliklə, korporativ üslub
                        şirkətin (firmanın) kimliyinin əks olunması, özünütanıtma mexanizmini (ilk təəssüratı) davam
                        etdirən fərqliliyidir.<br>

                        <b>Korporativ üslub məhsullarımız.</b><br>

                        Vizitkart, Buklet, Flayer Kataloq Zərf Blank Faktura Afişa Fayl, CD Promo məhsullar Korporativ
                        geyim və aksessuarlar Fərdi təqdimat kartları.

                        Müştərilər marka şəxsiyyətinizin peşəkar şəkildə dizayn və uyğun olmasını gözləyirlər. Marka
                        materiallarınızın köhnəlmiş hiss etməsini və ya vizual cazibə verməməsini istəmirsiniz. Bu,
                        işinizə pis təsir edə bilər. Bunun əvəzinə peşəkarlıqla ünsiyyət quran komandamız ilə çalışın.
                   ',
        'smm_mail' => 'E-poçt',
        'smm_mail_det' => '<b>E-poçt marketingi nədir?</b><br> Əsasən məhsulları və xidmətləri təklif etmək üçün e-poçt
                        istifadəsidir. Amma daha yaxşı bir e-poçt marketing anlayışı, potensial müştərilərlə əlaqələri
                        inkişaf etdirmək üçün elektron poçtdan istifadə edir. E-poçt marketinqi internet marketing
                        vasitələrindən , sosial şəbəkələrdən , bloglardan və s. vasitəsilə online marketinqi əhatə edən
                        bir seqmentdir. E-poçt marketinqinin ən böyük üstünlüyü müştərilərinizin sizin şirkətinizdə olan
                        təyişikliklər və kompaniyalar barəsində gündəlik məlumatlandırılmasdır.<span
                                id="dots3">...</span><span id="more3">

                            Bülletenlər, məsələn, bu potensial müştərilərə şirkətiniz haqqında xəbərlər, yenilikləri, qarşıdan gələn hadisələr və xüsusi təkliflərlə təminat verməklə, veb saytınızda lazımi informasiyanı təmin edən insanların yaratdığı e-poçt siyahısına göndərilə bilər. Bununla marka və xidmətlərinizi dahada dəyərini artıra bilərsiniz. İdeal olaraq, e-poçt marketing sosial media ilə əl-ələ keçməlidir. Sosial medianın <b>"Bəyən" və ya "Paylaş"</b> düymələrini marketinq e-poçtlarına əlavə etmək, müştərilərin markanızla əlaqə yaratmaq üçün əlavə bir yol verir. Sosial mediada müştərilərin müsbət rəyləri e-poçtlara daxil edilə bilər və əksinə, sosial media reklamları müştəriləri e-poçt xəbərlərinizə abunə olmağa təşviq etmək üçün istifadə edilə bilər. E-poct marketing xidmətləri ilə Deirvlon Technologies olaraq öndə gedən şirkətləri biz təmin edirik. Yeni marketing agentiniz <b> Deirvlon Technologies!</b>
                   ',
        'smm_google' => 'Google',
        'smm_google_det' => '2018-ci ilin iyun ayında Google marketinq məhsullarının Google Marketing Platforma
                        birləşdirildiyini elan etdi. Google-a görə, platforma “daha ​​ağıllı marketinq və daha yaxşı
                        nəticələr əldə etmək üçün vahid reklam və analitik platformadır”. Google Marketing Platform ,
                        Google-ın əsas marketinq məhsullarından ikisini birləşdirir:<b> Analytics 360 Suite və
                            DoubleClick.</b> Bu, Google üçün xarakterikdir, çünki şirkət inteqrasiya olunmuş və <span
                                id="dots2">...</span><span id="more2"> ardıcıl marketinq təcrübəsi yaratmaq məqsədini dəqiq bilirdi.  Google Marketin platformasi ayrıca daha böyük şirkətlər üçün ödənişli premium məhsulların yanında <b>pulsuz marketinq alətləri də təqdim edir.</b> Google Marketing Platform-un təkmilləşdirilmiş iş birliyi sayəsində şirkət və markalarınız böyütmək daha asandır .  Google Marketing Platformu ilə marketoloqlar və müəssisələr avtomatik olaraq bütün alətlərindən kritik anlayışlar alır və bu da auditoriyanızı daha yaxşı başa düşməyinizə kömək edir.  Və Bİz də <b> Derivlon Technologies</b> olaraq bu işləri sizin üçün dahada asanlaşdırırıq. Sizə fərqli şəkildə xidmət göstərməyə hazırıq.
                    ',
        'smm_youtube' => 'Youtube',
        'smm_youtube_det' => 'Youtube aylıq istifadəçi sayı artıq 2 milyardı ötüb. Sizin də bir şirkət kimi bu platformada
                        yerinizi almağınız vacibdir. Çünki insanlar hər hansı bir məlumata, məhsula və ya xidmətə
                        ehtiyac duyduqda görüntülü olaraq video şəkilində izləyərək daha az vaxt sərf edib daha ətraflı
                        məlumat almaq və ehtiyaclarını qarşılamağı məqsədə uyğun hesab edirlər. YouTube bundan düzgün və
                        davamlı istifadə edən müəssisələr üçün çox şey edə bilər . Bunu etmək üçün, YouTube
                        marketinqinin digər sosial kanallardan nə qədər fərqli olduğunu və bundan öz xeyrinizə necə
                        istifadə edəcəyinizi anlamalısınız .<span id="dots1">...</span><span id="more1"><br>   Video, marketinq dünyasına hakimdir və videodan istifadə etmirsinizsə, demək olar ki, rəqiblərinizə məğlub olacaqsınız. Bütün sosial platformalarda video sıralaması daha yüksək və reklamlarda yaxşı nəticə göstərən şirkətlərin digərlərinə nisbətən müştərilərin, daha çox görmə və cavab vermə ehtimalı yüksəkdir. YouTube istifadə edərkən bütün video kitabxananız olacaq. Daha sonra video faylları hər platformaya yerli olaraq yükləyə bilərsiniz.<b> Yalnız bir neçə klik ilə YouTube videolarını blog yazılarınıza yerləşdirə bilərsiniz,</b> bu da blog yazılarınızı daha dinamik və cəlbedici edir. Əgər bunları etmək sizin üçün vaxt itkisi kimi hesab olunursa, <b>Deirvlon Technologies</b> komandasi olaraq biz sizin üçün edərik. Ən yaxşı strategilayarla sizi rəqəmsal həyatla əlaqələndirəcəyik!
                    ',
        'smm_brend' => 'Brendinq',
        'smm_brend_det' => 'Əgər işinizdə marketinq strategiyası tələb olunursa , lakin hədəf auditoriyası ilə əlaqə qura
                        bilmirsizsə nə olacaq? Bir müəssisə məhsul və xidmətləri müştərilərin gözləntilərinə uyğun
                        olaraq çatdırır, lakin hədəf auditoriyası ilə müntəzəm olaraq əlaqələnmirsə və ya
                        əlaqələndirilmirsə ? Cavabın təxmin edilməsi sadədir ... bu biznes rəqabətçi onlayn bazarda
                        inkişaf etməkdə çətinliklərlə üzləşəcəkdir. Beləliklə, bu cür problemlərlə qarşılaşırsınızsa və
                        onlayn bazarda möhkəmlənmək istəsəniz, sizə fərqli şəkildə xidmət göstərməyə hazırıq, çünki
                        ehtiyaclarınıza əhəmiyyət veririk.<b> Siz uğur qazandıqca uğur qazanırıq.</b>',
        'smm_xid' => 'Xidmətlər',
        'smm_bra' => 'Branding',
        'smm_rek' => 'Reklam',
        'smm_kor' => 'Koorperativ üslub',
        'smm_geri' => 'Geri',
        'smm_read_more' => 'Read more',
        'dig' => 'Digərləri',
        'of' => 'Ofisə bax',
        'of2' => 'Caspian Support. bina C, offis 73.',
        'xer' => 'Xəritədən bax',
        'xer2' => 'Xəritədən bax',
        'fb' => '',
        'wp' => '',
        'ins' => '',
        'lin' => '',
        'you' => '',
        'en' => '',
        'pho' => '',
        'soc' => 'Social media',
        'mail' => 'Mail',
        'mail2' => 'salam@deirvlon.com ',
        'p1' => 'Phone',
        'p2' => '+994508747486',
        'p3' => '+994508747486',
        'loc1' => 'Office Location',
        'loc2' => 'Koroğlu pr. Bina Caspian Support.<br> mərtəbə 12, ofis 73 ',
        'c1' => 'Bizimlə əlaqə',
        'det' => 'Ətraflı',
        'menu_home' => 'Ana səhifə',
        'menu_serve' => 'Xidmətlər',
        'menu_blog' => 'Blog',
        'menu_app' => 'App & Games',
        'menu_media' => 'Mediada biz',
        'menu_partnor' => 'Tərəfdaşlar',
        'menu_contact' => 'Əlaqə',
        'cihaz' => 'Cihaz Təminatı',
        'cihaz_detail' => 'Biznesinizi böyütmək, işinizi inkişaf etdirmək üçün biz sizin texniki problemlərinizi avtomatlaşdıraraq işlərinizin gedişatını sürətləndirə, daha az vaxt və büdcə sərf etməyinizi təmin edə bilərik.',
        'proqram' => 'Proqram Təminatı',
        'proqram_detail' => 'Şirkətin rəqəmsal aləmdə varlığının təmini və rəqəmsal marketinq kompaniyalarının təmini və rəqəmsal marketinq kompaniyalarının təməli vebsaytdan başlayır. Şirkətin rəqəmsal aləmdə varlığının təmini və marketinq kompaniyalarının təməli vebsaytdan başlayır.',
        'smm' => 'SMM',
        'smm_desc' => 'Sadə dillə desək, sosial media marketinqi (SMM), auditoriyanızla əlaqə qurmaq üçün sosial media
                        platformalarının istifadəsidir. Ancaq sosial media marketinq proseduru o qədər də asan deyil.
                        Markanızın yaradılmasına, satışların artırılmasına və veb sayt trafikinizə kömək edirik. Bu
                        müddət sosial media profillərinizdə cəlbedici məzmun dərc etmək, görsəlliyi artırmaq,
                        izləyiciləri cəlb etmək və dinləmək və sosial media kampaniyaları və reklamları aparmaqla yanaşı
                        nəticələrini təhlil etməkdən ibarətdir. Sosial media platformalarından <span
                                id="dots">...</span><span id="more">(Facebook, İnstagram və s.) maksimum dərəcədə istifadə etməyinizə kömək edirik. <b> İstər işinizi böyütmək, istərsə də işinizi bir markaya çevirmək istəsəniz, müvəffəq olmağınıza kömək edəcəyik.</b> Sosial media, məzmunu yayımlamaq üçün platforma olmaqdan daha çox inkişaf etdi və bu səbəbdən də sosial medianı işinizə fayda gətirmək üçün müxtəlif yollarla istifadə edirik. Mütəxəssislərimiz sosial media danışıqlarınızı və müvafiq qeydlərə cavabınızı izləyir, əldə olunan məlumatları  təhlil edir və yüksək hədəfli sosial media kampaniyalarının aparılmasına kömək edir, markanıza destek verir və s. Bütövlükdə sosial media marketinqi idarəçiliyində mükəmməlliyik. Mükəmməl Partnyor ilə mükəmməl media, <b>Deirvlon Technologies!</b>',
        'smm_detail' => 'Şirkətin rəqəmsal aləmdə varlığının təmini və rəqəmsal marketinq kompaniyalarının təmini və rəqəmsal marketinq kompaniyalarının təməli vebsaytdan başlayır. Şirkətin rəqəmsal aləmdə varlığının təmini və marketinq kompaniyalarının təməli vebsaytdan başlayır.',
    ],
    'en' => ['apps_ot' => 'Other services',
        'apps_port' => 'Portfolio',
        'apps' => 'App',
        'apps_ab' => 'Information',
        'apps_h2' => 'We\'ll help you create the best online environments!',
        'apps_muted' => '<b>DISCOVER</b> creative applications with us <br>  that develop ideas you can\'t develop!</b>',
        'proq_serv' => 'Our services',
        'proq' => 'SOFTWARE',
        'proq_prod' => 'Our products',
        'proq_h2' => 'Our professionalism is for your success!',
        'proq_text_muted' => '<b>Apply now</b> for getting great results <br> with our special offers',
        'cihaz_prod' => 'Our products',
        'cihaz_h2' => 'Our professionalism is for your success!',
        'cihaz_text_muted' => '<b>Apply now</b> for getting great results <br> with our special offers',
        'smm_h2' => 'Our professionalism is for your success!',
        'smm_text_muted' => '<b>Apply now</b> for getting great results <br> with our special offers',
        'smm_korp' => 'Cooperative style',
        'smm_korp_det' => '<b>What is corporate style?</b><br>

                        Corporate style consists of visual, written, audio products that express the visual appearance of any company, the company\'s distinctive logo of colors and brands, the strategy of selection and memorization of the product or service, the structure of the commercial structure
                        style.<br>

                        <b>Importance of corporate style:</b><br>

The presence of a company in the market is characterized by its values ​​and ideology. The company needs different styles to stand out from the rest. Over time, the success of the organization will gain fame. Thus, the corporate style is a reflection of the identity of the company (firm), a distinction that continues the mechanism of self-promotion (first impression).<br>

                        <b>Our corporate style products.</b><br>

Business Card, Booklet, Flayer Catalog Envelope Blank Invoice Poster File, CD Promo Products Corporate Clothing and Accessories Individual presentation cards.

Customers expect your brand identity to be professionally designed and matched. You don\'t want your branded materials to feel outdated or visually unattractive. This, can adversely affect your work. Instead of, work with our professional team.
                   ',
        'smm_mail' => 'E-mail',
        'smm_mail_det' => '<b>What is email marketing?</b><br>Mostly companies use email to offer products and services.
        Email marketing - is the use of email primarily to offer products and services. But a better concept of email marketing uses email to develop relationships with potential customers. Email marketing is a segment that covers online marketing through internet marketing tools, social networks, blogs. The biggest advantage of email marketing is that your customers are informed about changes in your company and companies on a daily basis.<span
                                id="dots3">...</span><span id="more3">

                           Newsletters can be sent to an email list created by people who provide the information you need on your website, providing potential customers with news, updates, upcoming events, and special offers about your company. This will increase the value of your brand and services. Ideally, email marketing should go hand in hand with social media. Adding social media buttons to marketing emails gives customers an extra way to connect with your brand. Positive customer feedback on social media can be included in emails, and conversely, social media ads can be used to courage customers to subscribe to your email newsletter. We provide e-mail marketing services to leading companies as Deirvlon Technologies. Your new marketing agent is Deirvlon Technologies!</b>
                   ',
        'smm_google' => 'Google',
        'smm_google_det' => 'In June 2018, Google announced the integration of marketing products into the Google Marketing Platform. According to Google, the platform is “smarter marketing and  It is a single advertising and analytical platform for getting better results. ” Google Marketing Platform 
 combines two of Google\'s core marketing products:<b>Analytics 360 Suite and DoubleClick.</b>This is characterized for google,because the company <span
id="dots2">...</span><span id="more2"> knew exactly the purpose of creating an integrated and consistent marketing experience. The Google Marketing platform also offers <b>free marketing tools as well as paid premium products for larger companies.</b> Improved collaboration with Google Marketing Platform makes it easier to grow your company and brands. With the Google Marketing Platform, marketers and businesses automatically get critical insights from all their tools, which helps you better understand your audience. And we, as Derivlon Technologies, make it even easier for you. We are ready to serve you differently.
',
        'smm_youtube' => 'Youtube',
        'smm_youtube_det' => 'The number of monthly users of YouTube has already exceeded 2 billion. It is important that, your business is also on this platform. When people need any product or service they prefer to spend less time by watching video. Youtube can do a lot for who use youtube correctly and consistently. To do it, you must understand how different youtube marketing is working from other social channels and how to benefit from it. Video dominates the world of marketing, and if you don\'t use video, you will almost lose to your competitors. Companies with higher video rankings on all social platforms and with good results in ads are more likely to see and respond to customers than others. You will have video library when using YouTube. You can then upload the video files locally to each platform. With just a few clicks, you can upload YouTube videos to your blog posts, making your blog posts more dynamic and engaging. If doing this is considered a waste of time for you, as a Deirvlon Technologies team, we will do it for you. We will connect you to the digital life with the best strategies!
                    ',
        'smm_brend' => 'Brendinq',
        'smm_brend_det' => 'What if your business requires a marketing strategy but you can\'t connect with your target audience? An enterprise delivers products and services according to customers\' expectations, but does not communicate regularly with the target audience. The answer is simple ... this business will face difficulties in developing in a competitive online marketplace. So, if you face such problems and want to strengthen your position in the online market, we are ready to serve you differently, because we care about your needs. <b> The more you succeed, the more we do. </b>',
        'smm_xid' => 'Services',
        'smm_bra' => 'Branding',
        'smm_rek' => 'Advertisment',
        'smm_kor' => 'Corparate style',
        'smm_geri' => 'Back',
        'smm_read_more' => 'Read more',
        'dig' => 'Others',
        'of' => 'See office',
        'of2' => 'Caspian Support. building C, office 73.',
        'xer' => 'Go to map',
        'xer2' => 'Go to map',
        'fb' => '',
        'wp' => '',
        'ins' => '',
        'lin' => '',
        'you' => '',
        'en' => '',
        'pho' => '',
        'soc' => 'Social media',
        'mail' => 'Mail',
        'mail2' => 'salam@deirvlon.com ',
        'p1' => 'Phone',
        'p2' => '+994508747486',
        'p3' => '+994508747486',
        'loc1' => 'Office Location',
        'loc2' => 'Koroğlu pr. Bina Caspian Support.<br> mərtəbə 12, ofis 73 ',
        'c1' => 'Contact with us',
        'det' => 'Read more',
        'menu_home' => 'Home page',
        'menu_serve' => 'Services',
        'menu_blog' => 'Blog',
        'menu_app' => 'App & Games',
        'menu_media' => 'We are on media',
        'menu_partnor' => 'Partners',
        'menu_contact' => 'Contact',
        'cihaz' => 'Device Warranty',
        'cihaz_detail' => 'Ensuring the company\'s presence in the digital world and providing digital marketing companies and the foundation of digital marketing companies starts from the website.',
        'proqram' => 'Device Warranty',
        'proqram_detail' => 'Ensuring the company\'s presence in the digital world and providing digital marketing companies and the foundation of digital marketing companies starts from the website.',
        'smm' => 'SMM',
        'smm_desc' => 'Simply put, social media marketing (SMM) is the use of social media platforms to connect with your audience. However, the social media marketing procedure is not so easy. We help you build your brand, increase sales and your website traffic. During this time, you will be able to publish attractive content on your social media profiles, increase visibility, engage and listen to your audience, and conduct social media campaigns and ads. consists of analyzing the results.<span
                                id="dots">...</span><span id="more"> We help you make use of social media platforms (Facebook, Instagram, etc.). <b> Whether you want to grow your business or turn your business into a brand, we will help you succeed. </b> Social media has evolved more than just being a platform for content delivery, so we use social media in a variety of ways to benefit your business. . Our experts monitor your social media conversations and responses to relevant notes, analyze the information obtained and help you conduct high-targeted social media campaigns, support your brand, etc. Overall, we excel in social media marketing management. Perfect media with the Perfect Partner, <b> Deirvlon Technologies! </b> ',
        'smm_detail' => 'Ensuring the company\'s presence in the digital world and providing digital marketing companies and the foundation of digital marketing companies starts from the website.',],
];
function getLangFile($key, $sub)
{
    global $langFile;
    return $langFile[$key][$sub];
}