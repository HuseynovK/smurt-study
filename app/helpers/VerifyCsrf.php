<?php

use app\web\db;

function csrf_token()
{
    global $csrf_data;
    global $csrf_key;
//    var_dump($route + " " + $method);
//    die();
    $csrf = hash_hmac('ripemd160', $csrf_data, $csrf_key);

    $_SESSION['csrf_token'] = $csrf;

    return $csrf;
}

function generate_csrf_and_token($data = null)
{
    repated_token:

    global $csrf_data;

    $data = $data ? $data : $csrf_data;

    $csrf = encrypt($data);

    $query = "SELECT * FROM old_tokens where token=:token";

    $con = (db::connect())->prepare($query);

    $con->execute([
        "token" => $csrf
    ]);

    if ($con->fetch(PDO::FETCH_ASSOC)) {
        $query = "UPDATE old_tokens SET
                     status=:status
                      where token=:token";

        $con = (db::connect())->prepare($query);

        $con->execute([
            "status" => 1,
            "token" => $csrf
        ]);

        goto repated_token;
    }

    $query = "INSERT INTO old_tokens SET
                        token=:token";

    $con = (db::connect())->prepare($query);

    $con->execute([
        "token" => $csrf
    ]);

    $_SESSION['csrf_token'] = $csrf;

    return $csrf;
}

function generate_csrf_and_token2()
{
    global $csrf_key;

    $csrf = encrypt($csrf_key);
    
    return $csrf;
}


function check_csrf_and_token_verify($token)
{
    $query = "SELECT * FROM old_tokens where token=:token and status=:status";

    $con = (db::connect())->prepare($query);

    $con->execute([
        "token" => $token,
        "status" => 0,
    ]);

    if ($res = $con->fetch(PDO::FETCH_ASSOC)) {
        return [
            "code" => 0
        ];
    }

    return [
        "code" => 1
    ];
}

function end_csrf($token)
{
    $query = "UPDATE old_tokens SET
                     status=:status
                      where token=:token";

    $con = (db::connect())->prepare($query);

    $res = $con->execute([
        "token" => $token,
        "status" => 1,
    ]);

    if ($res) {
        return [
            "code" => 0
        ];
    }

    return [
        "code" => 1
    ];
}

function encrypt($data)
{
    global $csrf_key;

    $plaintext = $data;
    $ivlen = openssl_cipher_iv_length($cipher = "AES-256-CBC");
    $iv = openssl_random_pseudo_bytes($ivlen);
    $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $csrf_key, $options = OPENSSL_RAW_DATA, $iv);
    $hmac = hash_hmac('sha256', $ciphertext_raw, $csrf_key, $as_binary = true);
    $ciphertext = base64_encode($iv . $hmac . $ciphertext_raw);

    return $ciphertext;
}

function decrypt($ciphertext)
{
    global $csrf_key;

    $c = base64_decode($ciphertext);
    $ivlen = openssl_cipher_iv_length($cipher = "AES-256-CBC");
    $iv = substr($c, 0, $ivlen);
    $hmac = substr($c, $ivlen, $sha2len = 32);
    $ciphertext_raw = substr($c, $ivlen + $sha2len);
    $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $csrf_key, $options = OPENSSL_RAW_DATA, $iv);
    $calcmac = hash_hmac('sha256', $ciphertext_raw, $csrf_key, $as_binary = true);

    if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
    {
        return $original_plaintext;
    } else {
        return false;
    }
}
