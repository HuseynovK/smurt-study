<?php

function site_url($args)
{
    global $first_url;
    global $default_lang;

    if (!isset($_SESSION['lang'])) {
        $_SESSION['lang'] = $default_lang;
    } else {
        if (!$_SESSION['lang']) {
            $_SESSION['lang'] = $default_lang;
        }
    }

    return "$first_url/$_SESSION[lang]/" . $args;
}

function site_url_admin($args = false)
{
    global $first_url;
    return "$first_url/admin/" . $args;
}

function redirectBack($args = '')
{
    $_SESSION['back'] = $args;
    if (!array_key_exists("HTTP_REFERER", $_SERVER)) {
        global $first_url;
        session_write_close();
        header("Location: $first_url/admin/", true);
    }else{
        session_write_close();
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
//    var_dump($_SERVER["HTTP_REFERER"]);
//    die();
}

function assetUrl($url)
{
    global $asset_directory;

    return $asset_directory . $url;
}

function assetImage($url)
{
    global $root;

    return $root . '/' . $url;
}

function assetFileAdmin($url)
{
    global $root;

    return $root . '/app/views/admin/' . $url;
}

function assetFile($url)
{
    global $root;

    return $root . '/app/views/' . $url;
}
